#!/bin/sh

sed -i "s|__FEDERATION__|${FEDERATION}|g" /usr/share/nginx/html/main*.js
sed -i "s|__PROVIDER__|${PROVIDER}|g" /usr/share/nginx/html/main*.js
sed -i "s|__PROVIDER_VCS__|${PROVIDER_VCS}|g" /usr/share/nginx/html/main*.js

npx json-server /usr/local/app/api/db.json -i id --routes /usr/local/app/api/routes.json --host 0.0.0.0 --port 3000 &

nginx -g 'daemon off;'
