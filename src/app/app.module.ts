import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationModule } from './navigation/navigation.module';
import { MaterialModule } from './shared/material/material.module';
import { ServiceAdministrationModule } from './service-administration/service-administration.module';
import { ConfigService } from './core/services/config.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuestionnaireComponent } from './service-administration/components/questionnaire/questionnaire.component';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthModule } from './auth/auth.module';
import { KeycloakAngularModule, KeycloakBearerInterceptor, KeycloakService } from 'keycloak-angular';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { DataSetComponent } from './service-administration/components/dataset/dataset.component';
import { DatePipe } from '@angular/common';
import { DistributionComponent } from './service-administration/components/distribution/distribution.component';
import { environment } from '../environments/environment';

function initializeKeycloak(keycloak: KeycloakService) {
  return () =>
    keycloak.init({
      config: {
        url: environment.keycloak.url,
        realm: environment.keycloak.realm,
        clientId: environment.keycloak.clientId,
      },
      initOptions: {
        onLoad: 'check-sso',
        checkLoginIframe: false,
      },
      enableBearerInterceptor: true
    });
}

@NgModule({
  declarations: [AppComponent, QuestionnaireComponent, DataSetComponent, DistributionComponent],
  imports: [
    AuthModule,
    KeycloakAngularModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    MatListModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    NavigationModule,
    ServiceAdministrationModule,
    MaterialModule,
    MatDialogModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [ConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: loadConfigs,
      deps: [ConfigService],
      multi: true,
    },
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService]
    },
    {
      provide: MatDialogRef,
      useValue: {}
    }, { provide: MAT_DIALOG_DATA, useValue: {} }, {
      provide: KeycloakBearerInterceptor,
    },
    KeycloakService,
    [DatePipe],

  ],
  bootstrap: [AppComponent],
})

export class AppModule {
  constructor() {
  }
}

export function loadConfigs(
  config: ConfigService
) {
  return () =>
    new Promise<void>((resolve) => {
      config.load().then(() => resolve());
    });
}
