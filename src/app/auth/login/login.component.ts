import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { KeycloakService } from 'keycloak-angular';
import { CookieService } from 'ngx-cookie-service';
import jwt_decode from 'jwt-decode';
import {environment} from "../../../environments/environment";

@Component({
    selector: 'login-dialog',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    nameControl = new FormControl('');
    loading = true;
    error = false;
    constructor(private cookieService: CookieService, private keycloakService: KeycloakService, private formBuilder: FormBuilder, private router: Router, private _dialog: MatDialog) {
        environment.PROVIDER && router.navigate(['']);
        this.loginForm = this.formBuilder.group({
            selectedProvider: ['', [
                Validators.required,
            ]],
        });
    }
    ngOnInit(): void {
    }
    providers: any = [];

    public retrieveProvider(did: string) {
        const regex = /web:([\w-]+)\./;
        const match = did.match(regex);
        if (match && match[1]) {
            return match[1];
        } else {
            return environment.PROVIDER
        }
    }
    async onSubmit(): Promise<void> {
        try {
            this.nameControl.markAsTouched();
            await this.keycloakService.login().then((e) => console.log(e));
            const jwk = await this.keycloakService.getToken();
            this.cookieService.set('token', JSON.stringify(jwk));
            localStorage.setItem("decodedVP", JSON.stringify(jwt_decode(jwk)));
            //localStorage.setItem("logged", this.retrieveProvider(JSON.parse(localStorage.getItem("decodedVP")!).family_name));
            localStorage.setItem("logged", environment.PROVIDER);
            await this.router.navigate([""]);
            window.location.reload();
        } catch (error) {
            localStorage.removeItem("logged");
            this.router.navigate(["login"]);
        }
    }
}
