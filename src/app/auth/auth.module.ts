import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../shared/material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { CircularProgressBarModule } from '../shared/circular-progress-bar/circular-progress-bar.module';
import { StaticStarRatingModule } from '../shared/static-star-rating/static-star-rating.module';
import { OverlayModule } from '@angular/cdk/overlay';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        LoginComponent
    ],
    imports: [

        CommonModule,
        OverlayModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        MatToolbarModule,
        MatAutocompleteModule,
        CircularProgressBarModule,
        StaticStarRatingModule,
        MatSnackBarModule,
        MatTooltipModule,
        AuthRoutingModule,
    ]

})
export class AuthModule { }
