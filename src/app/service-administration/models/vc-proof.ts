export class VcProof {
    type: string =''
    created: string=''
    proofPurpose: string=''
    verificationMethod?: string=''
    jws: string =''
    domain?:string
}