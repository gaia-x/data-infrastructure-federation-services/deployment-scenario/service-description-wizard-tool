import { Component } from '@angular/core';
import { ServiceAdministrationService } from '../services/service-administration/service-administration.service';

@Component({
  template: ''
})

export abstract class ComplianceCriterion {
  apiCritResponse: any;
  typeCrit!: string;
  valueCrit!: boolean;

  constructor(private service: ServiceAdministrationService) {}

  ngOnInit() {
    this.service.getCritFromApi().subscribe(response => {
      this.apiCritResponse = response;
      this.typeCrit = "gax-compliance:hasCategory.@value";
      this.valueCrit = false
    });
  }
  }

