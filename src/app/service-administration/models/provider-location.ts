export class ProviderLocation {
  //selected: boolean = false;
  '@id': string = '';
  location_did: string ='';
  provider_designation: string =''
  country_name: string = '';
  state: string = '';
  urban_area: string = '';
  provider_did: string = ''

  constructor(obj?: Partial<ProviderLocation>) {
    if (obj) {
      Object.assign(this, obj);
    }
  }
}
