export class Claim {
  available: boolean = false;
  name: string = '';

  constructor(obj?: Partial<Claim>) {
    if (obj) {
      Object.assign(this, obj);
    }
  }
}
