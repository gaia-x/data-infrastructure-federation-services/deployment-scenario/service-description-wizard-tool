import { CredentialSubject } from "./credential-subject";
import { VcProof } from "./vc-proof";


export class VerifiableCredentialCertification {
    context : string [] = []
    id: string = '';
    type: string = '';
    issuer: string = '';
    credentialSubject!: CredentialSubject;
    proof?: VcProof;

    constructor(obj?: Partial<VerifiableCredentialCertification>) {
    if (obj) {
      Object.assign(this, obj);
    }
  }
}

