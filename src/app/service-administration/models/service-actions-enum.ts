export enum ServiceActionsEnum {
  UPLOAD_VP = 'Upload VP',
  DOWNLOAD_VC = 'Download VC',
  SUBMIT = 'Submit to Gaia-X for compliance',
  PUBLISH_CATALOG = 'Publish',
  PUBLISHED = 'Published on the catalog',
  REQUEST_LABEL = 'Request a label',
  EDIT = 'Edit service',
  SIGN = 'Sign claims',
  REQUEST_COMPLIANCE = 'Request compliance',
}
