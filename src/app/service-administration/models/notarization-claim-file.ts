export class NotarizationClaimFile {

    serviceOfferingId:string ='';
    certificationType:string =''
    issuer : string = ''
    credentialSubjectId=''
    b64file=''
    
    constructor(obj?: Partial<NotarizationClaimFile>) {
        if (obj) {
          Object.assign(this, obj);
        }
      }
}