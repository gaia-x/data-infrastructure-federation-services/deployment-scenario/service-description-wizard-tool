import { ComplianceReferenceClaim } from './compliance-reference-claim';
import { ProviderLocation } from './provider-location';
import { ServiceStatusEnum } from './service-status-enum';

export class ServiceDescription {

  // Mock format
  id: string = '';
  // service_title: string = '';
  status: ServiceStatusEnum = ServiceStatusEnum.DRAFT;
  compliance_label_level: number = 0;
  lastModified?: string = "2022-12-08T10:41:14.618Z";
  companyName: string = '';
  description?: string;
  termsAndConditions: string = "";
  federations?: string[];
  // otherCriteriaClaims?: string[];
  complianceReferenceClaims: ComplianceReferenceClaim[] = [];
  // locations?: ProviderLocation[];
  location: ProviderLocation = new ProviderLocation();
  // service_type : string[] = [];
  // layer : string = '';
  complianceQuestionnaire: boolean = false;
  locatedServiceOfferingVP?: any = '';
  serviceOfferingVP?: any = ''
  verifiablePresentation: any = ''
  verifiableCredential: any = ''
  processing: boolean = false;
  step: number = 0;
  locations:any = [];
  // New format
  questionnaire:any = []
  compliance_reference_title: string = "";
  country_name: string = "";
  keyword: string = "";
  layer: string = "";
  located_service_did: string = "";
  located_service_vc: string = "";
  location_did: string = "";
  location_provider_designation: string = "";
  provider_designation: string = "";
  provider_did: string = "";
  provider_service_id: string = "";
  service_did: string = "";
  service_title: string = "";
  service_type: any[] = [];
  state: string = "";
  urban_area: string = "";
  dataProduct: any = {}

  constructor(obj?: Partial<ServiceDescription>) {
    if (obj) {
      Object.assign(this, obj);
    }
  }
}
