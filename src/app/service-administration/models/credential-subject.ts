export class CredentialSubject  {
    id: string = '';
    certificationName: string ='';
    issuanceDate : string='';
    expirationDate: string='';

    constructor(obj?: Partial<CredentialSubject>) {
        if (obj) {
          Object.assign(this, obj);
        }
      }
}
