export class ProviderDesignation {
    provider_did: string = ''
    designation: string = ''
    
  constructor(obj?: Partial<ProviderDesignation>) {
    if (obj) {
      Object.assign(this, obj);
    }
  }
}