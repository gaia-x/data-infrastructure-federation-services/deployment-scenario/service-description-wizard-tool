export enum ServiceStatusEnum {
  DRAFT = 'Draft',
  READY_TO_SIGN='Ready to sign',
  READY_COMPLIANCE='Ready for compliance',
  NON_COMPLIANT = 'Non compliant format',
  COMPLIANT = 'Compliant',
  PUBLISHED = "Published"
}
