export class ComplianceReferenceClaimsByCategory {
  complianceCategory: string = '';
  complianceReferences: string[] = [];

  constructor(obj?: Partial<ComplianceReferenceClaimsByCategory>) {
    if (obj) {
      Object.assign(this, obj);
    }
  }
}
