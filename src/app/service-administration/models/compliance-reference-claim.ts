import { FileSummary } from "src/app/shared/file-upload/models/file-summary";

export class ComplianceReferenceClaim {
  compliance_ref_did: string ='';
  compliance_reference_manager : string ='';
  reference_sha256: string = '';
  reference_url: string = '';

  //Old ComplianceCategory
  type: string = '';
  //New ComplianceCategory
  'aster-conformity:referenceType': string = '';
  //Old title
  title: string = '';
  //New title
  'aster-conformity:hasComplianceReferenceTitle': string = '';

  valid_from_date: string = '';
  version: string = '';
  b64files: string[] = [];

  file: FileSummary | null = null;
  verifiableCredential : any;
  status: string = '';
  processing : boolean = false;

  constructor(type: string, title: string) {
    this.type = type;
    this.title = title;
  }
}
