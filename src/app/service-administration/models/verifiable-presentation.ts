import { VcProof } from "./vc-proof";
import { VerifiableCredentialCertification } from "./verifiable-credential-certification";

export class VerifiablePresentation {
    context: string[] = [];
    type:string = 'VerifiablePresentation'
    verifiableCredential!: VerifiableCredentialCertification[]
    proof?: VcProof
}