import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServiceAdministrationRoutingModule } from './service-administration-routing.module';
import { ServiceAdministrationComponent } from './components/service-administration/service-administration.component';
import { MaterialModule } from '../shared/material/material.module';
import { ServiceAdministrationFormComponent } from './components/service-administration-form/service-administration-form.component';
import { ServiceCreationComponent } from './components/service-creation/service-creation.component';
import { ServiceEditComponent } from './components/service-edit/service-edit.component';
import { ServiceVpUploadComponent } from './components/service-vp-upload/service-vp-upload.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FileUploadModule } from '../shared/file-upload/file-upload.module';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { CircularProgressBarModule } from '../shared/circular-progress-bar/circular-progress-bar.module';
import { StaticStarRatingModule } from '../shared/static-star-rating/static-star-rating.module';
import { AddReferenceDialogComponent } from './components/add-reference-dialog/add-reference-dialog.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { ViewServiceComponent } from './components/view-service/view-service.component';
import { SignClaimsComponent } from './components/sign-claims/sign-claims.component';
import { PublishComponent } from './components/publish/publish.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';



@NgModule({
  declarations: [
    ServiceAdministrationComponent,
    ServiceAdministrationFormComponent,
    ServiceCreationComponent,
    ServiceEditComponent,
    ServiceVpUploadComponent,
    AddReferenceDialogComponent,
    ViewServiceComponent,
    SignClaimsComponent,
    PublishComponent,
  ],
  imports: [
    CommonModule,
    OverlayModule,
    ServiceAdministrationRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    HttpClientModule,
    FileUploadModule,
    MatAutocompleteModule,
    CircularProgressBarModule,
    StaticStarRatingModule,
    MatSnackBarModule,
    MatTooltipModule,
  ],
  exports: [
    ServiceAdministrationComponent
  ]
})
export class ServiceAdministrationModule { }
