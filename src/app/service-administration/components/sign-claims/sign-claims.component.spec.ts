import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignClaimsComponent } from './sign-claims.component';

describe('SignClaimsComponent', () => {
  let component: SignClaimsComponent;
  let fixture: ComponentFixture<SignClaimsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SignClaimsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SignClaimsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
