import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { catchError, EMPTY } from 'rxjs';
import { FileSummary } from 'src/app/shared/file-upload/models/file-summary';
import { ServiceDescription } from '../../models/service-description';
import { ServiceAdministrationService } from '../../services/service-administration/service-administration.service';
import { VerifierService } from "../../services/verifier/verifier.service";

export interface DialogData {
  serviceDescription: ServiceDescription
}

@Component({
  selector: 'app-sign-claims',
  templateUrl: './sign-claims.component.html',
  styleUrls: ['./sign-claims.component.scss']
})
export class SignClaimsComponent implements OnInit {
  signatureMethod: string = '';
  VClists: any[] = [];
  vcDownloaded: number[] = [];
  signedVC: number[] = [];
  signatureStatus: {} = {};
  step1isValid = false;
  file!: FileSummary;
  VPFile!: FileSummary;
  constructor(
    public dialogRef: MatDialogRef<SignClaimsComponent>,
    private _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private serviceAdministrationService: ServiceAdministrationService,
    private verificationService: VerifierService
  ) { }

  onClose(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    this.step1isValid = false;
    this.data.serviceDescription.verifiablePresentation.verifiableCredential.forEach((vc: any) => {
      const status = vc.proof ? "Signed" : "Not signed";
      const obj = {"vc": vc, "status": status};
      !this.VClists.some(elem => {
        return JSON.stringify(obj) === JSON.stringify(elem);
      }) && this.VClists.push(obj)
      console.log(this.VClists)
    });
  }

  downloadVC(index: number): void {
    const credential = this.VClists.at(index).vc;
    const blob = new Blob([JSON.stringify(credential, null, 2)], {
      type: 'text/json',
    });
    const anchor = document.createElement('a');
    anchor.style.display = 'none';
    anchor.href = URL.createObjectURL(blob);
    anchor.download = `Verifiable_Credential_${this.getPresentationType(this.VClists[index].vc)}.json`;
    this.vcDownloaded.push(index);
    anchor.click();
  }

  VPsignature() {
    this.signatureMethod = "VP";
  }

  eachVCsignature() {
    this.signatureMethod = "VC";
  }

  downloadVPwithoutSignatures() {
    const blob = new Blob([JSON.stringify(this.data.serviceDescription.verifiablePresentation, null, 2)], {
      type: 'text/json',
    });
    const anchor = document.createElement('a');
    anchor.style.display = 'none';
    anchor.href = URL.createObjectURL(blob);
    anchor.download = `Verifiable_Presentation.json`;
    anchor.click();
  }

  downloadFinalVP() {
    let newVCs = this.VClists.map(obj => obj.vc)
    this.data.serviceDescription.verifiablePresentation.verifiableCredential = [];
    this.data.serviceDescription.verifiablePresentation.verifiableCredential = newVCs;
    // this.VPFile = this.data.serviceDescription.verifiablePresentation;

    const blob = new Blob([JSON.stringify(this.data.serviceDescription.verifiablePresentation, null, 2)], {
      type: 'text/json',
    });
    const anchor = document.createElement('a');
    anchor.style.display = 'none';
    anchor.href = URL.createObjectURL(blob);
    anchor.download = `Verifiable_Presentation.json`;
    anchor.click();


  }


  getButtonColor(status: string): string {
    switch (status) {
      case 'Signed':
        return 'green';
      case 'Not signed':
        return 'orange';
      case 'Error : signature not valid':
        return 'red';
      default:
        return 'gray';
    }
  }

  // Get the second type in the verifiableCredential
  getPresentationType(credential: any): string {
    if (credential.credentialSubject.type) {
      return credential.credentialSubject.type;
    }
    return '';
  }

  updateVP(file: FileSummary | Event) {
    this.VPFile = file as FileSummary;
    console.log(atob(this.VPFile.base64));
  }

  updateVC(file: FileSummary | Event, index: number) {
    var VC = file as FileSummary;
    try {
      let decodedVC = JSON.parse(atob(VC.base64))
      if (decodedVC.hasOwnProperty("proof")) {
        this.VClists[index].vc = decodedVC;
        this.VClists[index].status = "Signed";
      } else this.VClists[index].status = "Error : signature not valid";
    }
    catch (e) {
      this.openSnackBar('Invalid JSON file', 'Hide', {
        duration: 4000,
        panelClass: ['orange-snackbar'],
      });
    }
  }

  allVCsSigned() {
    return this.VClists.every(vc => vc.status === "Signed")
  }

  submitSignedDoc() {
    var enc = new TextDecoder("utf-8");
    let newVp: any;
    this.VPFile.content.then((res: any) => {
      newVp = enc.decode(res)
      try {
        newVp = JSON.parse(enc.decode(res))
      } catch (e) {
        this.openSnackBar('Invalid JSON file', 'Hide', {
          duration: 4000,
          panelClass: ['orange-snackbar'],
        });
      }

      this.data.serviceDescription.verifiablePresentation = newVp;
      let request = this.data.serviceDescription.verifiablePresentation

      this.verificationService.verifyVCs(newVp.verifiableCredential)
        .pipe(
          catchError(() => {
            this.openSnackBar('Error during VCs verification', 'Hide', {
              duration: 4000,
              panelClass: ['orange-snackbar'],
            });
            return EMPTY;
          })
        )
        .subscribe((verificationResult: Boolean) => {
          if (verificationResult) {
            this.serviceAdministrationService.postToStoreVP(request)
              .pipe(
                catchError(() => {
                  this.openSnackBar('Error storing VP', 'Hide', {
                    duration: 4000,
                    panelClass: ['orange-snackbar'],
                  });
                  return EMPTY;
                })
              ).subscribe((res: any) => {
              if (res.msg == 'OK') {
                this.openSnackBar('VP stored successfully', 'Hide', {
                  duration: 4000,
                  panelClass: ['blue-snackbar'],
                });
                this.dialogRef.close(this.data.serviceDescription);
              }
              else {
                this.openSnackBar('Error storing VP', 'Hide', {
                  duration: 4000,
                  panelClass: ['orange-snackbar'],
                });
              }
            })

            for(let credential of request.verifiableCredential) {
              this.serviceAdministrationService.postToStoreVC(credential)
                .pipe(
                  catchError(() => {
                    this.openSnackBar('Error storing VC', 'Hide', {
                      duration: 4000,
                      panelClass: ['orange-snackbar'],
                    });
                    return EMPTY;
                  })
                ).subscribe((res: any) => {
                if (res.msg == 'OK') {
                  this.openSnackBar('ServiceOffering VC stored successfully', 'Hide', {
                    duration: 4000,
                    panelClass: ['blue-snackbar'],
                  });
                }
                else {
                  this.openSnackBar('Error storing VC', 'Hide', {
                    duration: 4000,
                    panelClass: ['orange-snackbar'],
                  });
                }
              })
            }
          } else {
            this.openSnackBar('Error during VCs verification', 'Hide', {
              duration: 4000,
              panelClass: ['orange-snackbar'],
            });
          }
        });
    })
  }

  openSnackBar(message: string, action: string, type: any) {
    this._snackBar.open(message, action, type);
  }

  getElementByType(myArray: any, type: string) {
    for (const obj of myArray) {
      let content = JSON.stringify(obj);
      let json = JSON.parse(content);

      if (json.type.includes(type)) {
        return obj;
      }
    }
    return undefined;
  }

}
