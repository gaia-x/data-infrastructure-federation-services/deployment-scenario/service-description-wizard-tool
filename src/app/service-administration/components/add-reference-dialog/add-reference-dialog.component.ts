import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FileSummary } from 'src/app/shared/file-upload/models/file-summary';
import { ComplianceReferenceClaim } from 'src/app/service-administration/models/compliance-reference-claim';

export interface ReferenceDialogData {
  reference : ComplianceReferenceClaim,
  files : FileSummary[]
}

@Component({
  selector: 'app-add-reference-dialog',
  templateUrl: './add-reference-dialog.component.html',
  styleUrls: ['./add-reference-dialog.component.scss']
})
export class AddReferenceDialogComponent implements OnInit {

  file : FileSummary | undefined;

  constructor(
    public dialogRef: MatDialogRef<AddReferenceDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ReferenceDialogData,
  ) {}

  onClose(): void {
    this.dialogRef.close(this.data);
  }

  ngOnInit(): void {
  }

  updateVC(file : FileSummary) {
    this.file = file
    this.data.reference.file = file
  }

  updateSelfSignedVC(file: FileSummary) {
    this.file = file
    this.data.reference.file = file
  }

  updateCertification(file: FileSummary) {
    this.file = file
    this.data.reference.file = file
  }
}
