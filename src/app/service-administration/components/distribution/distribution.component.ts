import { Component, Inject, Injectable, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { PublishComponent } from '../publish/publish.component';

@Component({
    selector: 'distributionDialog',
    templateUrl: './distribution.component.html',
    styleUrls: ['./distribution.component.scss'],
})
@Injectable({
    providedIn: 'root',
})
export class DistributionComponent implements OnInit {
    distributionForm!: FormGroup;
    form: FormGroup = new FormGroup({});
    questionnaire: any;
  name!: string;
    category!: string;
    minObsoleteDate: string | null = null;
    maxObsoleteDate: string | null = null;
    minExpirationDate: string | null = null;



    constructor(
        public dialog: MatDialog,
        private fb: FormBuilder,
        public dialogRef: MatDialogRef<PublishComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {

    }

    onDelete(): void {
        this.dialogRef.close("delete")
    }

    ngOnInit(): void {

        this.distributionForm = this.fb.group(
            {
                identifier: new FormControl(''),
                algorithm: new FormControl('', [Validators.required]),
                title: new FormControl('', [Validators.required]),
                fileType: new FormControl(''),
                byteSize: new FormControl('', [Validators.required]),
                fileLocation: new FormControl('', [Validators.required]),
                fileHash: new FormControl(''),
                obsoleteDateTime: [null],
                expirationDateTime: [null],

            }, { validators: this.dateValidator }
        )
        if (this.data) {
            this.distributionForm.patchValue(this.data);
        }
        this.initializeDateConstraints();
    }


    hasError(controlName: string, errorName: string) {
        return this.distributionForm.controls[controlName].hasError(errorName);
    }


    dateValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
        if (!(control instanceof FormGroup)) {
            return null;
        }

        const issuanceDate = control.get('issuedDateTime');
        const obsoleteDate = control.get('obsoleteDateTime');
        const expirationDate = control.get('expirationDateTime');

        const issuanceDateObj = issuanceDate?.value ? new Date(issuanceDate.value) : null;
        const obsoleteDateObj = obsoleteDate?.value ? new Date(obsoleteDate.value) : null;
        const expirationDateObj = expirationDate?.value ? new Date(expirationDate.value) : null;

        if (issuanceDateObj && obsoleteDateObj) {
            const isObsoleteAfterIssuance = obsoleteDateObj.getTime() > issuanceDateObj.getTime();

            if (!isObsoleteAfterIssuance) {
                obsoleteDate?.setErrors({ obsoleteNotAfterIssuance: true });
            } else {
                obsoleteDate?.setErrors(null);
            }
        }

        if (issuanceDateObj && expirationDateObj) {
            const isIssuanceBeforeExpiration = issuanceDateObj.getTime() < expirationDateObj.getTime();

            if (!isIssuanceBeforeExpiration) {
                issuanceDate?.setErrors({ issuanceNotBeforeExpiration: true });
            } else {
                issuanceDate?.setErrors(null);
            }
        }

        if (expirationDateObj && obsoleteDateObj) {
            const isObsoleteBeforeExpiration = obsoleteDateObj.getTime() < expirationDateObj.getTime();

            if (!isObsoleteBeforeExpiration) {
                obsoleteDate?.setErrors({ obsoleteNotBeforeExpiration: true });
            } else {
                obsoleteDate?.setErrors(null);
            }
        }

        // this.initializeDateConstraints();

        return null;
    };


    initializeDateConstraints() {
        const today = new Date();
        const tomorrow = new Date(today);
        tomorrow.setDate(tomorrow.getDate() + 1);
        this.minObsoleteDate = tomorrow.toISOString().split('T')[0];

        this.distributionForm.get('obsoleteDateTime')?.valueChanges.subscribe((obsoleteDate: Date | null) => {
            if (obsoleteDate) {
                const adjustedObsoleteDate = new Date(obsoleteDate);
                adjustedObsoleteDate.setDate(adjustedObsoleteDate.getDate() + 2);
                this.minExpirationDate = adjustedObsoleteDate.toISOString().split('T')[0];

                const expirationDateControl = this.distributionForm.get('expirationDateTime');
                const currentExpirationDate = expirationDateControl?.value;

                if (currentExpirationDate && new Date(currentExpirationDate) <= new Date(obsoleteDate)) {
                    expirationDateControl?.setValue(null);
                }
            } else {
                this.minExpirationDate = null;
            }
        });
    }


    onDateChange(): void {
        const issuanceDate = this.distributionForm.get('issuedDateTime');
        const obsoleteDate = this.distributionForm.get('obsoleteDateTime');
        const expirationDate = this.distributionForm.get('expirationDateTime');
        if (issuanceDate && obsoleteDate && expirationDate) {
            this.distributionForm.updateValueAndValidity();
        }
    }

    onSubmit() {
        if (this.distributionForm.valid) {
            this.dialogRef.close(this.distributionForm);
            this.form.reset();
        }
    }

    onClose() {
        this.dialogRef.close()
    }

}
