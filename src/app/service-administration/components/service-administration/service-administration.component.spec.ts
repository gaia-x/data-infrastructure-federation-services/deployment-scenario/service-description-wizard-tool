import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceAdministrationComponent } from './service-administration.component';

describe('ServiceAdministrationComponent', () => {
  let component: ServiceAdministrationComponent;
  let fixture: ComponentFixture<ServiceAdministrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServiceAdministrationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ServiceAdministrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
