import {DatePipe, DOCUMENT} from '@angular/common';
import {HttpErrorResponse} from '@angular/common/http';
import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {MatDrawer} from '@angular/material/sidenav';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {Router} from '@angular/router';
import {KeycloakService} from 'keycloak-angular';
import {CookieService} from 'ngx-cookie-service';
import {catchError, EMPTY, Observable, Subscription} from 'rxjs';
import {ConfigService} from 'src/app/core/services/config.service';
import {ServiceActionsEnum} from '../../models/service-actions-enum';
import {ServiceDescription} from '../../models/service-description';
import {ServiceStatusEnum} from '../../models/service-status-enum';
import {ServiceAdministrationService} from '../../services/service-administration/service-administration.service';
import {SignClaimsComponent} from '../sign-claims/sign-claims.component';
import jwt_decode from 'jwt-decode';
import * as moment from 'moment';
import * as shajs from 'sha.js';
import {environment} from '../../../../environments/environment';
import { VerifierService } from "../../services/verifier/verifier.service";

@Component({
  selector: 'app-service-administration',
  templateUrl: './service-administration.component.html',
  styleUrls: ['./service-administration.component.scss'],
  providers: [DatePipe],
})
export class ServiceAdministrationComponent implements OnInit {
  @ViewChild('drawer') public drawer!: MatDrawer;

  @ViewChild(MatPaginator)
  public paginator!: MatPaginator;

//   @ViewChild(MatSort, { static: false }) sort!: MatSort;
  @ViewChild('empTbSort', {static: false}) empTbSort = new MatSort();


  displayedColumns = [
    'service_title',
    'location',
    'status',
    'label',
    'lastModified',
    'step',
    'actions',
  ];
  statusEnum: typeof ServiceStatusEnum = ServiceStatusEnum;
  actionsEnum: typeof ServiceActionsEnum = ServiceActionsEnum;
  dataSource: MatTableDataSource<ServiceDescription> = new MatTableDataSource();
  subscription!: Subscription;
  selectedServiceView: ServiceDescription = new ServiceDescription();
  loading: Observable<boolean>;
  sortedData: ServiceDescription[] = [];
  username: string = '';

  constructor(
    public dialogRef: MatDialogRef<SignClaimsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public serviceAdministrationService: ServiceAdministrationService,
    private router: Router,
    private dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private config: ConfigService,
    public keycloakService: KeycloakService,
    private cookieService: CookieService,
    private verificationService: VerifierService,
    @Inject(DOCUMENT) private document: Document
  ) {
    this.loading = this.serviceAdministrationService.getLoading();
    this.subscription = this.serviceAdministrationService.collection$.subscribe(
      (res) => {
        this.dataSource = new MatTableDataSource(res);
        setTimeout(() => {
            this.dataSource.sort = this.empTbSort;
          this.setFilters();
          this.setSortData();
          this.dataSource.paginator = this.paginator;
        }, 500);
      }
    );
  }

  close() {
    this.drawer.close();
  }

  private setFilters() {
    this.dataSource.filterPredicate = (
      data: ServiceDescription,
      filter: string
    ) => {
      return (
        data.service_title.toLowerCase().includes(filter) ||
        data.status.toLowerCase().includes(filter) ||
        data.location.country_name.toLowerCase().includes(filter) ||
        data.location.state.toLowerCase().includes(filter) ||
        data.location.urban_area.toLowerCase().includes(filter) ||
        data.step.toString().includes(filter)
      );
    };
  }

  private setSortData() {
    this.dataSource.sortData = (
        data: ServiceDescription[],
        sort: MatSort
    ) => {
        if (!sort.active || sort.direction === '') {
            return data;
        }

        return data.sort((a: ServiceDescription, b: ServiceDescription) => {
            let comparatorResult = 0;
            switch (sort.active) {
                case 'service_title':
                    comparatorResult = a.service_title.localeCompare(b.service_title);
                    break;
                case 'location':
                    comparatorResult = a.location.country_name.localeCompare(b.location.country_name);
                    break;
                case 'status':
                    comparatorResult = a.status.localeCompare(b.status);
                    break;
                case 'step':
                    comparatorResult = a.step >= b.step ? 1 : -1;
                    break;
                case 'lastModified':
                    comparatorResult = moment(a.lastModified).isSameOrBefore(moment(b.lastModified!)) ? -1 : 1;
                    break;
            }

            return comparatorResult * (sort.direction == 'asc' ? 1 : -1);
        });
    }
  }

  ngOnInit() {

    this.keycloakService.getToken().then((jwk: any) => {
      this.cookieService.set('token', JSON.stringify(jwk))
      localStorage.setItem("decodedVP", JSON.stringify(jwt_decode(jwk)));
      //this.username = this.getProvider(JSON.parse(localStorage.getItem("decodedVP")!).family_name)
      this.username = environment.PROVIDER;
      localStorage.setItem("logged", this.username);
    })
  }

  public getProvider(did: string) {
    const regex = /web:([\w-]+)\./;
    const match = did.match(regex);
    if (match && match[1]) {
      return match[1];
    } else {
      return "dufourstorage"
    }

  }
  public applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  navigateToCreation() {
    this.router.navigateByUrl('/create');
  }

  navigateToEditPage(serviceDescription: ServiceDescription) {
    this.router.navigateByUrl(`/${serviceDescription.id}`);
  }

  delete(serviceDescription: ServiceDescription) {
    this.serviceAdministrationService
      .delete(serviceDescription.id)
      .subscribe((res) => console.log(res));
  }

  getStatusLabelClass(status: string, label: string) {
    switch (status) {
      case this.statusEnum.COMPLIANT:
        return 'status-compliant';
      case this.statusEnum.DRAFT:
        return 'status-draft';
      case this.statusEnum.PUBLISHED:
        if (label != '') return 'status-published-label';
        else return 'status-published-nolabel';
      case this.statusEnum.READY_COMPLIANCE:
      case this.statusEnum.READY_TO_SIGN:
        return 'status-ready';
      default:
        return '';
    }
  }

  getProgressionStage(status: ServiceStatusEnum, label: number) {
    switch (status) {
      case this.statusEnum.DRAFT:
        return 0;
      case this.statusEnum.READY_TO_SIGN:
        return 1;
      case this.statusEnum.READY_COMPLIANCE:
        return 2;
      case this.statusEnum.COMPLIANT:
        if (label != 0) return 4;
        else return 3;
      case this.statusEnum.PUBLISHED:
        if (label != 0) return 5;
        else return 4;
      default:
        return 0;
    }
  }

  defineProgressionStage(service: ServiceDescription) {
    service.step = this.getProgressionStage(service.status, service.compliance_label_level)
    return service.step
  }

  viewService(service: ServiceDescription) {
    this.selectedServiceView = service;
  }

  requestCompliance(service: ServiceDescription) {
    service.processing = true;
    let request = service.verifiablePresentation;

    this.serviceAdministrationService
      .postToCompliance(request)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          console.log(error);
          service.processing = false;
          this.openSnackBar(
            'Service could not be submitted to compliance',
            'Hide',
            {
              duration: 4000,
              panelClass: ['orange-snackbar'],
            }
          );
          return EMPTY;
        })
      )
      .subscribe((res: any) => {
        service.processing = false;
        service.status = this.statusEnum.COMPLIANT;
        service.verifiablePresentation['verifiableCredential'].push(res);
        this.serviceAdministrationService.updateServiceDescription(service.id, service).subscribe(() => {
            this.openSnackBar(
                `Service ${service.service_title} submitted to compliance verification`,
                'Hide',
                {
                  duration: 5000,
                  panelClass: ['green-snackbar'],
                }
            );
        });

        let request = service.verifiablePresentation;
        this.verificationService.verifyVCs(request.verifiableCredential)
          .pipe(
            catchError(() => {
              this.openSnackBar('Error during VCs verification', 'Hide', {
                duration: 4000,
                panelClass: ['orange-snackbar'],
              });
              return EMPTY;
            })
          )
          .subscribe((verificationResult: Boolean) => {
            if (verificationResult) {
              this.serviceAdministrationService.postToStoreVP(request)
                .pipe(
                  catchError((error: HttpErrorResponse) => {
                    console.log(error)
                    this.openSnackBar('Error storing VP', 'Hide', {
                      duration: 4000,
                      panelClass: ['orange-snackbar'],
                    });
                    return EMPTY;
                  })
                ).subscribe((res: any) => {
                if (res.msg == 'OK') {
                  this.openSnackBar('VP stored successfully', 'Hide', {
                    duration: 4000,
                    panelClass: ['blue-snackbar'],
                  });
                }
                else {
                  this.openSnackBar('Error storing VP', 'Hide', {
                    duration: 4000,
                    panelClass: ['orange-snackbar'],
                  });
                }
              });
            } else {
              this.openSnackBar('Error during VCs verification', 'Hide', {
                duration: 4000,
                panelClass: ['orange-snackbar'],
              });
            }
          });
      });
  }

  private downloadVC(service: ServiceDescription) {
    const blob = new Blob(
      [JSON.stringify(service.verifiableCredential, null, 2)!],
      {
        type: 'text/json',
      }
    );
    const anchor = document.createElement('a');
    anchor.style.display = 'none';
    anchor.href = URL.createObjectURL(blob);
    anchor.download = `VC_${service.service_title}.json`;
    anchor.click();
  }

  getLabelUrl(service: ServiceDescription) {
    let url = environment.WEB_CATALOGUE_URL + '/labelling';
    let vp_did = service.verifiablePresentation['@id'];
    if (vp_did != undefined) {
      url += '/' + vp_did.replaceAll('/', '%2F');
    } else {
      return "";
    }
    return url;
  }

  goToSimulateLabel(service: ServiceDescription, event: Event) {
    const url = this.getLabelUrl(service);
    event.preventDefault();
    if (!url) {
      this.openSnackBar(
        'Label for Service could not be simulate',
        'Hide',
        {
          duration: 5000,
          panelClass: ['orange-snackbar'],
        }
      );
      return;
    }
    window.open(url, '_blank');
  }

  requestLabel(service: ServiceDescription) {
    service.processing = true;
    let request = service.verifiablePresentation;
    this.serviceAdministrationService
      .postToLabelling(request)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          console.log(error);
          service.processing = false;
          this.openSnackBar(
            'Service could not be submitted to labeling',
            'Hide',
            {
              duration: 4000,
              panelClass: ['orange-snackbar'],
            }
          );
          return EMPTY;
        })
      )
      .subscribe((res: any) => {
        if (res && res.Error) {
          service.processing = false;
          this.openSnackBarButton(
            'You do not meet all the necessary criteria to have a label',
            'Simulate Label',
            {
              duration: 10000,
              panelClass: ['orange-snackbar'],
            },
            service
          );
        } else {
          service.processing = false;
          service.status = this.statusEnum.COMPLIANT;
          service.verifiableCredential = res;
          service.verifiablePresentation['verifiableCredential'].push(service.verifiableCredential)
          let label = res['credentialSubject']['label']['hasName'].toString();
          const regex = /Level (\d+)/;
          const match = label.match(regex);
          service.compliance_label_level = match ? match[1] : null;

          this.serviceAdministrationService.updateServiceDescription(service.id, service).subscribe(() => {
            this.openSnackBar(
              `Gaia-X label for service ${service.service_title} estimated to level ${service.compliance_label_level}`,
              'Hide',
              {
                duration: 5000,
                panelClass: ['green-snackbar'],
              }
            );
          });

          let request = service.verifiablePresentation;
          this.verificationService.verifyVCs(request.verifiableCredential)
            .pipe(
              catchError(() => {
                this.openSnackBar('Error during VCs verification', 'Hide', {
                  duration: 4000,
                  panelClass: ['orange-snackbar'],
                });
                return EMPTY;
              })
            )
            .subscribe((verificationResult: Boolean) => {
              if (verificationResult) {
                this.serviceAdministrationService.postToStoreVP(request)
                  .pipe(
                    catchError((error: HttpErrorResponse) => {
                      console.log(error)
                      this.openSnackBar('Error storing VP', 'Hide', {
                        duration: 4000,
                        panelClass: ['orange-snackbar'],
                      });
                      return EMPTY;
                    })
                  ).subscribe((res: any) => {
                  if (res.msg == 'OK') {
                    this.openSnackBar('VP stored successfully', 'Hide', {
                      duration: 4000,
                      panelClass: ['blue-snackbar'],
                    });
                  } else {
                    this.openSnackBar('Error storing VP', 'Hide', {
                      duration: 4000,
                      panelClass: ['orange-snackbar'],
                    });
                  }
                });
              } else {
                this.openSnackBar('Error during VCs verification', 'Hide', {
                  duration: 4000,
                  panelClass: ['orange-snackbar'],
                });
              }
            });
        }
      });

  }

  getLastModificationDays(lastModified: string) {
    let date = this.toDate(lastModified);
    let currentDate = new Date();
    return Math.floor(
      (currentDate.getTime() - date.getTime()) / 1000 / 60 / 60 / 24
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  toDate(date: string) {
    return new Date(date);
  }

  getDate(date: string) {
    return moment(date).format('DD/MM/YY, HH:mm');
  }

  formatDateString(lastModified: string) {
    let days = this.getLastModificationDays(lastModified);
    if (days == 0) return 'Today';
    else if (days == 1) return days + ' day ago';
    else if (days > 1 && days <= 31) return days + ' days ago';
    else if (days >= 31 && days < 365)
      return Math.floor(days / 30) + ' month(s) ago';
    else if (days >= 365) return Math.floor(days / 365) + ' year(s) ago';

    return 'Never';
  }

  duplicate(service: ServiceDescription) {
    this.serviceAdministrationService.duplicateService(service).subscribe(() => {});
  }

  openSignDialog(service: ServiceDescription): void {
    const dialogRef = this.dialog.open(SignClaimsComponent, {
      width: '90%',
      minHeight: '45vh',
      data: { serviceDescription: service },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        service.status = this.statusEnum.READY_COMPLIANCE;
        this.serviceAdministrationService.updateServiceDescription(service.id, service).subscribe(() => {
            this.openSnackBar(
                `Successfully uploaded signed verifiable presentation`,
                'Hide',
                {
                  duration: 5000,
                  panelClass: ['green-snackbar'],
                }
            )
        })
      }
    });
  }

  openSnackBar(message: string, action: string, type: any) {
    this._snackBar.open(message, action, type);
  }

  openSnackBarButton(message: string, action: string, type: any, service: ServiceDescription) {
    let snackBarRef = this._snackBar.open(message, action, type);

    snackBarRef.onAction().subscribe(() => {
      window.open(this.getLabelUrl(service), '_blank');
    });
  }

  publish(service: ServiceDescription) {
    service.federations?.push("abc-federation");
      this.serviceAdministrationService
        .pushToProviderCatalogue(service.id, service)
        .subscribe(() =>
          this.openSnackBar(
            `Service ${service.service_title} published in the following federations : ${service.federations}`,
            'Hide',
            {
              duration: 5000,
              panelClass: ['green-snackbar'],
            }
          )
        );
    service.processing = false;
    service.status = this.statusEnum.PUBLISHED;
  }
}
