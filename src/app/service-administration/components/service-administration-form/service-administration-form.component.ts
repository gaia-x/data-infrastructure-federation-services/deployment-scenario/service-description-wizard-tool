import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, OnDestroy} from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import {Router} from '@angular/router';
import {ServiceDescription} from '../../models/service-description';
import {ServiceStatusEnum} from '../../models/service-status-enum';
import {catchError, EMPTY, map, Observable, startWith,} from 'rxjs';
import {ComplianceReferenceClaim} from '../../models/compliance-reference-claim';
import {animate, AUTO_STYLE, state, style, transition, trigger,} from '@angular/animations';
import {ServiceAdministrationService} from '../../services/service-administration/service-administration.service';
import {City, Country, ICity, ICountry, IState, State,} from 'country-state-city';
import {ProviderLocation} from '../../models/provider-location';
import {MatDialog} from '@angular/material/dialog';
import {AddReferenceDialogComponent} from '../add-reference-dialog/add-reference-dialog.component';
import {ConfigService} from 'src/app/core/services/config.service';
import {QuestionnaireComponent} from '../questionnaire/questionnaire.component';
import * as shajs from 'sha.js';
import {MatSnackBar} from '@angular/material/snack-bar';
import {DataSetComponent} from '../dataset/dataset.component';
import {DatePipe} from '@angular/common';
import * as CryptoJS from 'crypto-js';
import {urlValidator} from '../../services/resolver/url-validator';
import {ResolverService} from "../../services/resolver/resolver.service";
import {environment} from '../../../../environments/environment';
import { FileSummary } from 'src/app/shared/file-upload/models/file-summary';

@Component({
  selector: 'app-service-administration-form',
  templateUrl: './service-administration-form.component.html',
  styleUrls: ['./service-administration-form.component.scss'],
  animations: [
    trigger('collapse', [
      state('false', style({ height: AUTO_STYLE, visibility: AUTO_STYLE })),
      state('true', style({ height: '0', visibility: 'hidden' })),
      transition('false => true', animate(200 + 'ms ease-in')),
      transition('true => false', animate(200 + 'ms ease-out')),
    ]),
  ],
})
export class ServiceAdministrationFormComponent implements OnInit {
  questionnaire: any;
  Object = Object;
  @Input()
  serviceDescription!: ServiceDescription;
  @Input()
  buttonIcon!: string;
  @Input()
  buttonText!: string;
  @Input()
  buttonLoading!: boolean;
  @Input()
  disabled!: boolean;
  @Output()
  public submitted = new EventEmitter<ServiceDescription>();

  VPFile!: FileSummary;
  inputText = new FormControl('');
  textList: string[] = [];
  loading: boolean = true;
  form!: FormGroup;
  statusEnum: typeof ServiceStatusEnum = ServiceStatusEnum;
  serviceLayers: string[] = [];
  service_type: string[] = [];
  aggregationOf: string[] = [];
  locations: ProviderLocation[] = [];
  federations: string[] = [];
  claimsReferences: ComplianceReferenceClaim[] = [];
  countries: ICountry[] = Country.getAllCountries();
  states!: IState[];
  cities!: ICity[];
  filteredCountries: Observable<ICountry[]>;
  filteredStates!: Observable<IState[]>;
  filteredCities!: Observable<ICity[]>;
  participantId: String = '';
  locationsForm!: FormGroup;
  dataProductForm!: FormGroup;
  showAddClaimForm: boolean = false;
  companyName: string = '';
  generatingVP: boolean = false;
  showAddLocation: boolean = false;
  showEditNameForm: boolean = false;
  licenses: string[] = [];
  datasetHashes: string[] = [];
  minObsoleteDate: string | null = null;
  maxObsoleteDate: string | null = null;
  minExpirationDate: string | null = null;
  changedQuestionnaire: boolean = false;
  termsAndConditionsHash: string = '';

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private datePipe: DatePipe,
    private fb: FormBuilder,
    private router: Router,
    private serviceAdministrationService: ServiceAdministrationService,
    //private questionnaireComponent: QuestionnaireComponent,
    public dialog: MatDialog,
    private config: ConfigService,
    private _snackBar: MatSnackBar,
    private resolverService: ResolverService
  ) {
    this.loadData();
    let p_designation = environment.PROVIDER;
    if (p_designation) this.companyName = p_designation;

    this.form = this.fb.group({
      id: new FormControl(''),
      service_title: new FormControl(''),
      description: new FormControl(''),
      termsAndConditions: ['', {
        validators: [Validators.required],
        asyncValidators: [urlValidator(resolverService)],
        updateOn: 'blur'
      }],
      service_type: new FormControl([]),
      layer: new FormControl(''),
      lastModified: new FormControl(''),
      availableLocations: new FormControl([]),
      location: new FormControl('', Validators.required),
      state: new FormControl(''),
      urban_area: new FormControl(''),
      country_name: new FormControl(''),
      federations: new FormControl(['Gaia-X', 'ABC-federation']),
      complianceReferenceClaims: new FormControl([]),
      serviceOfferingVP: new FormControl(''),
      locatedServiceOfferingVP: new FormControl(''),
      verifiableCredential: new FormControl(''),
      verifiablePresentation: new FormControl(''),
      provider_designation: new FormControl(''),

    });

    (this.locationsForm = this.fb.group({
      countryForm: new FormControl('', [Validators.required]),
      stateForm: new FormControl('', []),
      urbanAreaForm: new FormControl('', [Validators.required]),
    }));
      (this.filteredCountries = this.locationsForm.controls[
        'countryForm'
      ].valueChanges.pipe(
        startWith(null),
        map((country) => {
          return country ? this._filterCountries(country) : this.countries.slice()
        }
        )
      ));

    this.serviceAdministrationService.questionnaire$.subscribe((res) => {
      this.questionnaire = res;
    });

    this.dataProductForm = this.fb.group(
      {
        // aggregationof: new FormControl([]),
        aggregationof: this.fb.array([], Validators.required),
        issuedDateTime: [null],
        obsoleteDateTime: [null],
        expirationDateTime: [null],
        dataController: new FormControl(''),
        consent: new FormControl(''),
        dataDomains: new FormControl('', []),
        licenses: new FormControl('', []),
        identifier: new FormControl('', Validators.required),
        endpointURI: new FormControl(''),
        policies: new FormControl('', []),
        exposedThrough: new FormControl('', Validators.required)
      }, { validators: this.dateValidator }
    )

    this.form.get('termsAndConditions')?.valueChanges.subscribe(url => {
      this.resolverService.checkUrl(url).subscribe(isResolvable => {
        if (isResolvable) {
          this.resolverService.checkUrlContent(url).subscribe(content => {
            this.termsAndConditionsHash = CryptoJS.SHA256(content).toString();
          });
        } else {
          this.termsAndConditionsHash = "";
        }
      });
    });
  }

  ngOnInit(): void {

    this.initializeDateConstraints();
    if (
      this.serviceDescription.location.country_name != "" && this.serviceDescription.location.urban_area != "" && this.serviceDescription.location.state != "" && !this.locations.some(
        (item) =>
          item.country_name === this.serviceDescription.location.country_name &&
          item.state === this.serviceDescription.location.state &&
          item.urban_area === this.serviceDescription.location.urban_area
      )
    ) {
      this.locations.push(
        new ProviderLocation(this.serviceDescription.location)
      );
    }
    if (this.serviceDescription.id) this.populateFormOnEdit();
    else {
      this.serviceDescription = new ServiceDescription();
      this.claimsReferences.forEach((r) => {
        r.status = '';
        r.file = null;
      });
    }
    if (this.loading) this.form.disable;
    this.loading = false;
    if (!this.serviceDescription.verifiablePresentation) //this.createVp();

      if (!this.serviceDescription.serviceOfferingVP) {
        //TODO replace when we remove id in method
        //this.createSo();
      }

      else this.editSo();
  }

  ngOnDestroy() {
    this.changedQuestionnaire = false;
  }

  hasError(controlName: string, errorName: string) {
    return this.dataProductForm.controls[controlName].hasError(errorName);
  }

  onConsentChange(event: any) {
    this.dataProductForm.value.consent = (event.checked ? 'true' : 'false');
  }

  addDomain(inputRef: HTMLInputElement): void {
    if (inputRef.value !== '') {
      const dataDomains = this.dataProductForm.get('dataDomains') as FormControl;
      const currentValue = dataDomains.value as string[];
      const newValue = [...currentValue, inputRef.value];
      dataDomains.setValue(newValue);
      inputRef.value = '';
    }
  }

  addPolicy(inputRef: HTMLInputElement): void {
    if (inputRef.value !== '') {
      const policies = this.dataProductForm.get('policies') as FormControl;
      const currentValue = policies.value as string[];
      const newValue = [...currentValue, inputRef.value];
      policies.setValue(newValue);
      inputRef.value = '';
    }
  }

  addLicense(inputRef: HTMLInputElement): void {
    if (inputRef.value !== '') {
      const licenses = this.dataProductForm.get('licenses') as FormControl;
      const currentValue = licenses.value as string[];
      const newValue = [...currentValue, inputRef.value];
      licenses.setValue(newValue);
      inputRef.value = '';
    }
  }


  removeDomain(index: number): void {
    const dataDomains = this.dataProductForm.get('dataDomains') as FormControl;
    const currentValue = dataDomains.value as string[];
    const newValue = currentValue.filter((_, i) => i !== index);
    dataDomains.setValue(newValue);
  }

  removePolicy(index: number): void {
    const policies = this.dataProductForm.get('policies') as FormControl;
    const currentValue = policies.value as string[];
    const newValue = currentValue.filter((_, i) => i !== index);
    policies.setValue(newValue);
  }

  removeLicense(index: number): void {
    const licenses = this.dataProductForm.get('licenses') as FormControl;
    const currentValue = licenses.value as string[];
    const newValue = currentValue.filter((_, i) => i !== index);
    licenses.setValue(newValue);
  }

  dateValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    if (!(control instanceof FormGroup)) {
      return null;
    }

    const issuanceDate = control.get('issuedDateTime');
    const obsoleteDate = control.get('obsoleteDateTime');
    const expirationDate = control.get('expirationDateTime');

    const issuanceDateObj = issuanceDate?.value ? new Date(issuanceDate.value) : null;
    const obsoleteDateObj = obsoleteDate?.value ? new Date(obsoleteDate.value) : null;
    const expirationDateObj = expirationDate?.value ? new Date(expirationDate.value) : null;

    if (issuanceDateObj && obsoleteDateObj) {
      const isObsoleteAfterIssuance = obsoleteDateObj.getTime() > issuanceDateObj.getTime();

      if (!isObsoleteAfterIssuance) {
        obsoleteDate?.setErrors({ obsoleteNotAfterIssuance: true });
      } else {
        obsoleteDate?.setErrors(null);
      }
    }

    if (issuanceDateObj && expirationDateObj) {
      const isIssuanceBeforeExpiration = issuanceDateObj.getTime() < expirationDateObj.getTime();

      if (!isIssuanceBeforeExpiration) {
        issuanceDate?.setErrors({ issuanceNotBeforeExpiration: true });
      } else {
        issuanceDate?.setErrors(null);
      }
    }

    if (expirationDateObj && obsoleteDateObj) {
      const isObsoleteBeforeExpiration = obsoleteDateObj.getTime() < expirationDateObj.getTime();

      if (!isObsoleteBeforeExpiration) {
        obsoleteDate?.setErrors({ obsoleteNotBeforeExpiration: true });
      } else {
        obsoleteDate?.setErrors(null);
      }
    }

    // this.initializeDateConstraints();

    return null;
  };
  private verifiableCredentials: any;


  initializeDateConstraints() {
    const today = new Date();
    const tomorrow = new Date(today);
    tomorrow.setDate(tomorrow.getDate() + 1);
    this.minObsoleteDate = tomorrow.toISOString().split('T')[0];

    this.dataProductForm.get('obsoleteDateTime')?.valueChanges.subscribe((obsoleteDate: Date | null) => {
      if (obsoleteDate) {
        const adjustedObsoleteDate = new Date(obsoleteDate);
        adjustedObsoleteDate.setDate(adjustedObsoleteDate.getDate() + 2);
        this.minExpirationDate = adjustedObsoleteDate.toISOString().split('T')[0];

        const expirationDateControl = this.dataProductForm.get('expirationDateTime');
        const currentExpirationDate = expirationDateControl?.value;

        if (currentExpirationDate && new Date(currentExpirationDate) <= new Date(obsoleteDate)) {
          expirationDateControl?.setValue(null);
        }
      } else {
        this.minExpirationDate = null;
      }
    });
  }


  onDateChange(): void {
    const issuanceDate = this.dataProductForm.get('issuedDateTime');
    const obsoleteDate = this.dataProductForm.get('obsoleteDateTime');
    const expirationDate = this.dataProductForm.get('expirationDateTime');
    if (issuanceDate && obsoleteDate && expirationDate) {
      this.dataProductForm.updateValueAndValidity();
    }
  }


  isConsentChecked(): boolean {
    return this.dataProductForm.value.consent === 'Yes';
  }
  public getDataSetVisible() {
    return this.form.value.service_type.includes("Data Product")
  }

  private populateFormOnEdit() {
    this.form.controls['id'].patchValue(this.serviceDescription.id);
    this.form.controls['service_title'].patchValue(
      this.serviceDescription.service_title
    );

    // this.serviceDescription.locations.length != 0 && (this.locations = this.serviceDescription.locations);
    this.form.controls['location'].patchValue(this.serviceDescription.location);

    this.form.controls['provider_designation'].patchValue(
      this.serviceDescription.provider_designation
    );
    this.form.controls['description'].patchValue(
      this.serviceDescription.description
    );
    this.form.controls['termsAndConditions'].patchValue(
      this.serviceDescription.termsAndConditions
    );
    this.form.controls['service_type'].patchValue(
      this.serviceDescription.service_type
    );
    this.form.controls['layer'].setValue(this.serviceDescription.layer);
    this.form.controls['federations'].patchValue(
      this.serviceDescription.federations
    );
    this.form.controls['lastModified'].patchValue(
      this.serviceDescription.lastModified
    );
    this.form.controls['complianceReferenceClaims'].patchValue(
      this.serviceDescription.complianceReferenceClaims
    );
    this.form.controls['serviceOfferingVP'].patchValue(
      this.serviceDescription.serviceOfferingVP
    );
    this.form.controls['locatedServiceOfferingVP'].patchValue(
      this.serviceDescription.locatedServiceOfferingVP
    );
    this.form.controls['verifiablePresentation'].patchValue(
      this.serviceDescription.verifiablePresentation
    );
    this.form.controls['verifiableCredential'].patchValue(
      this.serviceDescription.verifiableCredential
    );


    this.dataProductForm.controls['expirationDateTime'].patchValue(
      this.serviceDescription.dataProduct.expirationDateTime
    );
    this.dataProductForm.controls['obsoleteDateTime'].patchValue(
      this.serviceDescription.dataProduct.obsoleteDateTime
    );

    this.dataProductForm.controls['consent'].patchValue(
      this.serviceDescription.dataProduct.consent
    );
    this.dataProductForm.controls['dataController'].patchValue(
      this.serviceDescription.dataProduct.dataController
    );
    this.dataProductForm.controls['dataDomains'].patchValue(
      this.serviceDescription.dataProduct.dataDomains
    );
    this.dataProductForm.controls['policies'].patchValue(
      this.serviceDescription.dataProduct.policies
    );


    this.questionnaire = this.serviceDescription.questionnaire;

    const aggregationOfArray = this.dataProductForm.controls['aggregationof'] as FormArray;

    this.serviceDescription.dataProduct.aggregationof.forEach((dataset: any) => {
      const datasetFormGroup = this.fb.group({
        copyrightOwnedBy: [dataset.copyrightOwnedBy],
        identifier: [dataset.identifier],
        description: [dataset.description],
        termsAndConditions: [dataset.termsAndConditions],
        personalDataPolicy: [dataset.personalDataPolicy],
        license: [dataset.license],
        obsoleteDateTime: [dataset.obsoleteDateTime],
        expirationDateTime: [dataset.expirationDateTime],
        title: [dataset.title],
        distributions: [dataset.distributions]
      });
      aggregationOfArray.push(datasetFormGroup);
    });
  }

  private loadData() {
    this.serviceAdministrationService.serviceLayers$.subscribe(
      (res) => (this.serviceLayers = res)
    );

    let storedLocations = localStorage.getItem("locations");
    this.locations = storedLocations ? JSON.parse(storedLocations) : [];

    this.serviceAdministrationService.service_type$.subscribe(
      (res) => (this.service_type = res)
    );

    this.serviceAdministrationService.federations$.subscribe(
      (res) => (this.federations = res)
    );
    this.serviceAdministrationService.claimsReferences$.subscribe((res) => {
      this.claimsReferences = res;
    });
  }


  addText() {
    const newText = this.inputText.value;
    if (newText && !this.textList.includes(newText)) {
      this.textList.push(newText);
      this.inputText.setValue('');
    }
  }
  filterClaimsBy(category: string) {
    return this.claimsReferences.filter((p) => {
        return p['aster-conformity:referenceType'] == category;
    })
  }

  addClaimsReference(reference: ComplianceReferenceClaim) {
    if (!this.serviceDescription.complianceReferenceClaims)
      this.serviceDescription.complianceReferenceClaims = [];
    this.serviceDescription.complianceReferenceClaims.push(reference);
    this.form.controls['complianceReferenceClaims'].patchValue(
      this.serviceDescription.complianceReferenceClaims
    );
  }

  private _filterCountries(value: string): ICountry[] {
    const filterValue = value.toLowerCase();
    this.locationsForm.controls['stateForm'].patchValue('');
    this.locationsForm.controls['urbanAreaForm'].patchValue('');
    return this.countries.filter((country) =>
      country.name.toLowerCase().includes(filterValue)
    );
  }

  private _filterStates(value: string): IState[] {
    const filterValue = value.toLowerCase();
    return this.states.filter((state) =>
      state.name.toLowerCase().includes(filterValue)
    );
  }

  private _filterCities(value: string): ICity[] {
    const filterValue = value.toLowerCase();
    return this.cities.filter((city) =>
      city.name.toLowerCase().includes(filterValue)
    );
  }

  setservice_title(event: any) {
    this.form.value.service_title = event.target.value;
    // this.form.controls['service_title'] = event.target.value
    this.serviceDescription.service_title = this.form.value.service_title;
  }

  getState() {
    if (this.locationsForm.controls['countryForm'].valid) {
      let countryName = this.locationsForm.controls['countryForm'].value;
      let country = this.countries.find(
        (country) => country.name == countryName
      );

      this.states = State.getStatesOfCountry(country?.isoCode);

      this.filteredStates = this.locationsForm.controls[
        'stateForm'
      ].valueChanges.pipe(
        startWith(null),
        map((state) => {
            return state ? this._filterStates(state) : this.states.slice()
        }
        )
      );
    }
  }

  getUrbanArea() {
    setTimeout(() => {
        if (this.locationsForm.controls['stateForm'].valid) {
            let stateName = this.locationsForm.controls['stateForm'].value;
            let state = this.states.find((state) => {return state.name == stateName});
            let countryName = this.locationsForm.controls['countryForm'].value;
            let country = this.countries.find(
              (country) => country.name == countryName
            );
            this.cities = City.getCitiesOfState(country!.isoCode, state!.isoCode);
            this.filteredCities = this.locationsForm.controls[
              'urbanAreaForm'
            ].valueChanges.pipe(
              startWith(null),
              map((city) =>
                city ? this._filterCities(city) : this.cities.slice()
              )
            );
        }
    }, 200);
  }

  addLocation() {
    let countryName: string = this.locationsForm.controls['countryForm'].value;
    let state: string = this.locationsForm.controls['stateForm'].value;
    let urbanArea: string = this.locationsForm.controls['urbanAreaForm'].value;

    let location: ProviderLocation | undefined;
    if (this.locations && this.locations.length > 0) {
      location = this.locations.find((loc: ProviderLocation) =>
        loc.country_name === countryName &&
        loc.state === state &&
        loc.urban_area === urbanArea
      );
    } else {
      location = undefined;
    }

    if (!location) {
      this.locations = this.locations || [];
      this.locations.push(
            new ProviderLocation({
              country_name: this.locationsForm.controls['countryForm'].value,
              state: this.locationsForm.controls['stateForm'].value,
              urban_area: this.locationsForm.controls['urbanAreaForm'].value,
            })
        );
    }
    this.locationsForm.reset();
  }

  navigateBack() {
    this.router.navigateByUrl('/');
  }

  editName() {
    this.showEditNameForm = !this.showEditNameForm;
    if (!this.form.controls['service_title'].value)
      this.form.controls['service_title'].patchValue(
        this.serviceDescription.service_title
      );
  }

  addClaim() {
    if (!this.serviceDescription.complianceReferenceClaims) {
      this.serviceDescription.complianceReferenceClaims = [];
    }
    this.serviceDescription.complianceReferenceClaims.push(
      new ComplianceReferenceClaim(
        this.form.controls['type'].value,
        this.form.controls['title'].value
      )
    );
    this.showAddClaimForm = false;
  }

  checkFormValidity() {
    let valid = true;
    if (this.serviceDescription.complianceReferenceClaims) {
      this.serviceDescription.complianceReferenceClaims.forEach((res) => {
        if (!res.file || (res.file && !(res.status == 'Signed'))) valid = false;
      });
      if (this.form.value.service_type.includes('Data Product')) {
        this.dataProductForm.value.aggregationof.length == 0 && (valid = false)
      }
    }
    valid =
    //   valid && this.form.valid &&((this.serviceDescription.service_title != '' && this.form.dirty) || (this.serviceDescription?.status === this.statusEnum.DRAFT && this.serviceDescription.service_title != '') || (this.changedQuestionnaire && this.serviceDescription.service_title != ''));
        valid && this.form.valid &&(((this.form.dirty || this.serviceDescription?.status === this.statusEnum.DRAFT || this.changedQuestionnaire) && this.serviceDescription.service_title != ''));
    return valid;
  }

  checkFormValidityForDraft() {
    return !this.serviceDescription.service_title;

  }

  checkLocation(location: ProviderLocation) {
    return (
      this.serviceDescription.location.urban_area == location.urban_area &&
      this.serviceDescription.location.state == location.state &&
      this.serviceDescription.location.country_name == location.country_name
    );
  }

  checkFederation(federation: string) {
    return (
      this.serviceDescription.federations?.includes(federation) ||
      this.isDefaultFederation(federation)
    );
  }

  isDefaultFederation(federation: string) {
    return federation.toLowerCase() == 'gaia-x';
  }

  uploadReferences() { }

  openDialog(reference: ComplianceReferenceClaim): void {
    const dialogRef = this.dialog.open(AddReferenceDialogComponent, {
      width: '90%',
      minHeight: 'calc(100vh - 90px)',
      height: 'auto',
      data: { service_title: this.serviceDescription, reference: reference },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.createVp();
      // if (!this.serviceDescription.locatedServiceOfferingVP)
      //   vp = this.createVp();
      // else vp = this.createVp();
      if (result.reference.file) {
        result.reference.processing = true;
        let vc = this.form.value.verifiableCredential;
        if (vc == '') {
          this.sendToNotarizationForCertificateClaimVC(
            {
              participant: this.companyName,
              certificationType: result.reference.title + " " + (result.reference.version != "" && result.reference.version),
              // add version
              b64file: result.reference.file.base64,
              serviceOffering: this.editSo(),
              // or DID
              federation: "abc-federation"
            },
            result.reference
          );
        } else {
          // shouldn't have this anymore
          this.sendToNotarizationForCertificateClaimVC(
            {
              participant: this.companyName,
              certificationType: result.reference.title,
              b64file: result.reference.file.base64,
              serviceOffering: this.editSo(),
              locatedServiceOffering: this.form.value.verifiableCredential,
            },
            result.reference
          );
        }
      }
    });
  }

  sendToNotarization(request: any, reference: any) {
    this.serviceAdministrationService
      .postToNotarization(request)
      .pipe(
        catchError(() => {
          this.openSnackBar('Error generating VC', 'Hide', {
            duration: 4000,
            panelClass: ['orange-snackbar'],
          });
          reference.processing = false;
          return EMPTY;
        })
      )
      .subscribe((res) => {
        if (res.result == 'success') {
          this.serviceDescription.verifiablePresentation[
            'verifiableCredential'
          ] = res.vc;
          this.form.controls['verifiableCredential'].patchValue(res.vc);
          this.serviceDescription.verifiableCredential = res.vc;
          this.form.value.verifiablePresentation =
            this.serviceDescription.verifiablePresentation;
          let reference =
            this.serviceDescription.complianceReferenceClaims.find(
              (res) => res.title == request.certificationType
            )!;
          reference.status = 'Signed';
          this.openSnackBar('VC generated successfully', 'Hide', {
            duration: 4000,
            panelClass: ['green-snackbar'],
          });
        }
        reference.processing = false;
      });
  }


  sendToNotarizationForCertificateClaimVC(request: any, reference: any) {
    this.serviceAdministrationService
      .postToNotarizationForCertificateClaimVC(request)
      .pipe(
        catchError(() => {
          this.openSnackBar('Error generating VC', 'Hide', {
            duration: 4000,
            panelClass: ['orange-snackbar'],
          });
          reference.processing = false;
          return EMPTY;
        })
      )
      .subscribe((res) => {
        if (res.result == 'success') {
          this.serviceDescription.verifiablePresentation[
            'verifiableCredential'
          ].push(res.vc);
          this.serviceDescription.verifiableCredential = res.vc
          this.form.controls['verifiableCredential'].patchValue(res.vc);


          this.form.value.verifiablePresentation =
            this.serviceDescription.verifiablePresentation;
          let reference =
            this.serviceDescription.complianceReferenceClaims.find(
              (res) => res.title + " " + (res.version != "" && res.version) == request.certificationType
            )!;
          reference.status = 'Signed';
          this.openSnackBar('VC generated successfully', 'Hide', {
            duration: 4000,
            panelClass: ['green-snackbar'],
          });
        }
        reference.processing = false;
      });
  }
  sendToNotarizationForVPgeneration(request: any) {
    this.generatingVP = true;
    this.serviceAdministrationService
      .postToNotarizationForVP(request)
      .pipe(
        catchError(() => {
          this.generatingVP = false;
          this.openSnackBar('Error generating VP', 'Hide', {
            duration: 4000,
            panelClass: ['orange-snackbar'],
          });
          return EMPTY;
        })
      )
      .subscribe((res) => {
        if (res.result == 'success') {
          this.generatingVP = false;
          this.serviceDescription.verifiablePresentation = res.vp;
          this.form.value.verifiablePresentation = res.vp;
          this.openSnackBar('VP generated successfully', 'Hide', {
            duration: 4000,
            panelClass: ['green-snackbar'],
          });
        }
      });
  }

  getParticipantName() {
    return this.config.getCompanyName().replace(/\s/g, '').toLowerCase();
  }

  onEnterKey(event: Event): void {
    event.preventDefault();
  }

  openDataSetForm(values?: any) {
    const dialogRef = this.dialog.open(DataSetComponent, {
      data: values,
      minWidth: '60vw',
      maxWidth: '60vw'
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        if (result === "delete") {
          this.removeDataset(values);
          // console.log(values)
          // const foundIndex = this.dataProductForm.value.aggregationof.findIndex((item: any) => item.hash === values.hash);
          // if (foundIndex !== -1) {
          //   this.dataProductForm.value.aggregationof = this.dataProductForm.value.aggregationof.filter((item: any) => item.hash !== values.hash);
          //   console.log("OOOK", this.dataProductForm.value.aggregationOf)
        }
        else if (result) {
          this.addDataSet(result);
        }
      }
    })
  }

  removeDataset(values: any) {
    const foundIndex = this.aggregationof.value.findIndex((item: any) => item.identifier === values.identifier);
    if (foundIndex !== -1) {
      this.aggregationof.removeAt(foundIndex);
    }
  }

  get aggregationof(): FormArray {
    return this.dataProductForm.get('aggregationof') as FormArray;
  }

  updateAggregationof() {
    const aggregationofControl = this.dataProductForm.get('aggregationof');
    const updatedAggregationof = [...aggregationofControl!.value];
    aggregationofControl!.setValue(updatedAggregationof);
  }

  addDataSet(form: any) {
    const values = form.value;
    if (values.identifier === "") {
      const generatedUuid = this.createId(values.title);
      const dataSet = { ...values };
      dataSet.identifier = generatedUuid;
      this.datasetHashes.push(generatedUuid);
      this.aggregationof.push(this.fb.control(dataSet));
    } else {
      const identifierToUpdate = values.identifier;
      const foundIndex = this.aggregationof.value.findIndex((element: any) => element.identifier === identifierToUpdate);
      if (foundIndex !== -1) {
        const updatedObject = { ...this.aggregationof.value[foundIndex], ...values };
        this.aggregationof.at(foundIndex).setValue(updatedObject);
      }
    }
  }

  openQuestionnaire() {
    if (this.serviceDescription.questionnaire)
      localStorage.setItem(
        'questionnaire',
        JSON.stringify(this.serviceDescription.questionnaire)
      );

    const dialogRef = this.dialog.open(QuestionnaireComponent, {
      width: '90vw',
      minHeight: '90vh',
      height: 'auto',
      data: this.questionnaire || {},
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.changedQuestionnaire = true;
        this.questionnaire = JSON.parse(localStorage.getItem('questionnaire')!);
        this.form.value.questionnaire = this.questionnaire;
      }
    });
  }

  deleteClaim(claim: ComplianceReferenceClaim) {
    let arr = this.form.controls['complianceReferenceClaims'].value;
    const index = arr.indexOf(claim, 0);
    if (index > -1) {
      arr.splice(index, 1);
    }
  }

  getClaimsIds(questionnaire: any) {
    let criterions:any = [];
    var bCriterions = this.questionnaire
      .filter((criterion: any) => criterion.value === true)
      .map((criterion: any) => ({id: criterion.id}))


    if (bCriterions.length != 0) {
        bCriterions.forEach((claim: any) => {
            let dids = claim.id.split(', ');
            dids.forEach((did: string) => {
                criterions.push({id: did});
            })
        });
    }

    return criterions;
  }

  createCritVC() {
    let formValue = this.form.value;
    let participantId = this.createId(this.companyName);
    let sacc = this.createId((Math.random() + 1).toString(36).substring(7));
    formValue.lastModified = new Date().toISOString();

    let criterions:any = this.getClaimsIds(this.questionnaire);



    return {
      "@context": [
        `https://schema-registry.${environment.FEDERATION}.demo23.gxfs.fr/contexts/aster-conformity`
      ],
      'id': `https://${this.companyName}.provider.demo23.gxfs.fr:participant:${participantId}/self-assessed-compliance-criteria-claim/${sacc}/data.json`,
      type: 'VerifiableCredential',
      issuer: `https://${this.companyName}.provider.demo23.gxfs.fr`,
      credentialSubject: {
        "aster-conformity:hasServiceOffering": this.serviceDescription.serviceOfferingVP['id'],
        "aster-conformity:hasAssessedComplianceCriteria": criterions
        //"type": "aster-conformity:SelfAssessedComplianceCriteriaClaim"
      },
    };
  }

  downloadVPTemplate() {
    const filePath = 'assets/template/vp.json';
    const fileName = 'vp.json';
    const link = document.createElement('a');

    link.href = filePath;
    link.download = fileName;
    document.body.appendChild(link);

    link.click();
    document.body.removeChild(link);
  }

  downloadVp() {
    if (this.form.value.verifiableCredential !== '') {
      this.sendToNotarizationForVPgeneration({
        participant: this.companyName,
        locatedServiceOffering: this.form.value.verifiableCredential,
        questionnaire: this.questionnaire,
      });
    } else {
      this.createVp();
    }

    const blob = new Blob(
      [JSON.stringify(this.form.value.verifiablePresentation, null, 2)!],
      {
        type: 'text/json',
      }
    );
    const anchor = document.createElement('a');
    anchor.style.display = 'none';
    anchor.href = URL.createObjectURL(blob);
    anchor.download = `VP_${this.serviceDescription.service_title}.json`;
    anchor.click();
  }

  createVp() {
    let participantId = this.createId(this.companyName);

    let formValue = this.form.value;
    //random ids
    let vpid = this.createId((Math.random() + 1).toString(36).substring(7));
    formValue.lastModified = new Date().toISOString();

    this.createSo();
    let currentDate: Date = new Date();

    let vp = {
      '@context': [
        'https://www.w3.org/2018/credentials/v1',
        'https://w3id.org/security/suites/jws-2020/v1',
        'https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#'
      ],
      '@id': `https://${this.companyName}.provider.demo23.gxfs.fr:participant:${participantId}/vp/${vpid}/data.json`,
      '@type': ['VerifiablePresentation'],
      'expirationDate': this.formatDate(new Date(currentDate.getTime() + 90 * 24 * 60 * 60 * 1000)),
      verifiableCredential: [
        this.serviceDescription.serviceOfferingVP,
        this.createCritVC()
      ],
    };
    const vcsArray = JSON.parse(environment.PROVIDER_VCS);
    vcsArray.forEach((vc: any) => {
      vp.verifiableCredential.push(vc);
    });

    this.form.controls['verifiablePresentation'].patchValue(vp);
    this.serviceDescription.verifiablePresentation = vp;

    return vp;
  }

  createSo() {
    this.participantId = this.createId(this.companyName);
    let formValue = this.form.value;
    let formattedServiceTypes = this.extractServiceTypes(formValue);
    //random ids
    let soid = this.createId((Math.random() + 1).toString(36).substring(7));
    formValue.lastModified = new Date().toISOString();
    let currentDate: Date = new Date();

    let id = `https://${this.companyName}.provider.demo23.gxfs.fr:participant:${this.participantId}/vc/${soid}/data.json`;
    let so = {
      "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://w3id.org/security/suites/jws-2020/v1",
        "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
      ],
      // '@id': id,
      'id' : id,
      'type': [
        'VerifiableCredential'
      ],
      'issuer': `https://${this.companyName}.provider.demo23.gxfs.fr`,
      'expirationDate': this.formatDate(new Date(currentDate.getTime() + 90 * 24 * 60 * 60 * 1000)),
      'credentialSubject': {
        'id': id,
        'type': 'gx:ServiceOffering',
        'gx:providedBy': {
          'id': `https://${this.companyName}.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/data.json`
        },
        'gx:termsAndConditions': {
          "gx:URL": formValue.termsAndConditions,
          "gx:hash": this.termsAndConditionsHash
        },
        'gx:name': formValue.service_title,
        'aster-conformity:layer': formValue.layer,
        'gx:keyword': formattedServiceTypes,
        'gx:description': formValue.description,
        'gx:descriptionMarkDown': '',
        'gx:webAddress': '',
        'gx:dataProtectionRegime' : ["GDPR2016"],
        'gx:dataAccountExport': [
          {
            "gx:requestType": "email",
            "gx:accessType": "digital",
            "gx:formatType": "mime/png"
          }
        ],
        'gx:isAvailableOn': [
          formValue.location.location_did
        ],
        "gx:policy": "default: allow",
        "@context": [
          "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#",
          `https://schema-registry.${environment.FEDERATION}.demo23.gxfs.fr/contexts/aster-conformity`
        ]
      }
    };

    if (formattedServiceTypes.includes("Data Product")) {
      so = this.editSoForDataProduct(so);
    }

    this.serviceDescription.serviceOfferingVP = so;
    return so;
  }


  editSoForDataProduct(so: any) {
    let dataProductValue = this.dataProductForm.value;

    // Delete Type 'ServiceOfferingExperimental' and Add type 'gx-data:DataProduct'
    /*
    const index = so.type.indexOf('ServiceOfferingExperimental');
    if (index > -1) {
      so.type.splice(index, 1);
    }
    so.type.push('gx-data:DataProduct');
    */

    // Add Context gx-data
    so["@context"].push({ "aster-data": `https://schema-registry.${environment.FEDERATION}.demo23.gxfs.fr/contexts/credentials` });

    // Add type DataProduct in credentialSubject
    // so!["credentialSubject"]["@type"] = 'gx-data:DataProduct';
    so!["credentialSubject"]["type"] = 'gx:ServiceOffering';

    // Add DataDomains
    so!["credentialSubject"]["gx-data:dataDomains"] = dataProductValue.dataDomains;

    // Add DataController
    so!["credentialSubject"]["gx-data:dataController"] = dataProductValue.dataController;

    // Add Consent - TODO : boolean to Array
    so!["credentialSubject"]["gx-data:consent"] = dataProductValue.consent;

    // Add Policies
    // so!["credentialSubject"]["gx-data:policies"] = dataProductValue.policies; OLD
    so!["credentialSubject"]["gx:policies"] = dataProductValue.policies;



    // Add Date
    so!["credentialSubject"]["gx-data:obsoleteDateTime"] = dataProductValue.obsoleteDateTime;
    so!["credentialSubject"]["gx-data:expirationDateTime"] = dataProductValue.expirationDateTime;

    interface DistributionInfo {
      "title": string;
      "dct:format": string;
      "dcat:byteSize": number;
      "gx:location": string;
      "gx:hash": string;
      "gx:hashAlgorithm": string;
      "gx:expirationDateTime": string;
      "gx:obsoleteDateTime": string;
    }

    interface AggregationInfo {
      "dct:identifier": string;
      "gx:title": string;
      "gx:description": string;
      "gx:copyrightOwnedBy": string;
      "dct:license": string[];
      "gx:personalDataPolicy": string;
      "gx:expirationDateTime": string;
      "gx:obsoleteDateTime": string;
      "dct:distribution": DistributionInfo[];
    }

    let aggregationOf: AggregationInfo[] = [];

    for (let i in dataProductValue.aggregationof) {
      let aggregation: AggregationInfo = {
        "dct:identifier": dataProductValue.aggregationof[i].identifier,
        "gx:title": dataProductValue.aggregationof[i].title,
        "gx:description": dataProductValue.aggregationof[i].description,
        "gx:copyrightOwnedBy": dataProductValue.aggregationof[i].copyrightOwnedBy,
        "dct:license": dataProductValue.aggregationof[i].licenses,
        "gx:personalDataPolicy": dataProductValue.aggregationof[i].personalDataPolicy,
        "gx:expirationDateTime": dataProductValue.aggregationof[i].expirationDateTime,
        "gx:obsoleteDateTime": dataProductValue.aggregationof[i].obsoleteDateTime,
        "dct:distribution": []
      };

      let distributions = dataProductValue.aggregationof[i].distributions;

      distributions.forEach((distribution: any) => {
        let fileInfo: DistributionInfo = {
          "title": distribution.title,
          "dct:format": distribution.fileType,
          "dcat:byteSize": distribution.byteSize,
          "gx:location": distribution.fileLocation,
          "gx:hash": distribution.fileHash,
          "gx:hashAlgorithm": distribution.algorithm,
          "gx:expirationDateTime": distribution.expirationDateTime,
          "gx:obsoleteDateTime": distribution.obsoleteDateTime
        };
        aggregation["dct:distribution"].push(fileInfo);
      });

      aggregationOf.push(aggregation);
    }
    so!["credentialSubject"]["gx:aggregationOf"] = aggregationOf;
    return so;
  }

  createId(val: string) {
    return shajs('sha256').update(val).digest('hex');
  }

  editSo() {
    let formValue = this.form.value;
    let sovp: any = this.serviceDescription.serviceOfferingVP;
    //if modifications, change ID
    let formattedServiceTypes: any[];
    formattedServiceTypes = this.extractServiceTypes(formValue);
    formValue.service_title = this.serviceDescription.service_title;
    formValue.locations = this.serviceDescription.locations;
    this.serviceDescription.provider_designation = this.companyName;
    formValue.lastModified = new Date().toISOString();
    if (!formValue.status) formValue.status = ServiceStatusEnum.DRAFT;
    // DANGER => WAS STATE
    formValue.state = this.serviceDescription.state;

    // let lsovp : any = this.serviceDescription.locatedServiceOfferingVP

    //types

    if (!sovp['credentialSubject']) {
      sovp!['gx:keyword'] = formattedServiceTypes;

      //layer
      sovp!['aster-conformity:layer'] = formValue.layer;

      this.serviceDescription.complianceReferenceClaims.forEach((element) => {
        if (element.verifiableCredential)
          this.verifiableCredentials.push(element.verifiableCredential);
      });

      //lsovp["verifiableCredential"] = verifiableCredentials

      //service_title
      sovp!['gx:name'] = this.serviceDescription.service_title;

      //description
      sovp!['gx:description'] = formValue.description;

      sovp!['gx:termsAndConditions']['gx:URL'] = formValue.termsAndConditions;
      sovp!['gx:termsAndConditions']['gx:hash'] = this.termsAndConditionsHash;

      //location
      sovp!['gx-service-offering:isAvailableOn'][0] = formValue.location.location_did;
    } else {
      sovp!['credentialSubject']['gx:keyword'] = formattedServiceTypes;

      //layer
      sovp!['credentialSubject']['aster-conformity:layer'] = formValue.layer;

      this.serviceDescription.complianceReferenceClaims.forEach((element) => {
        if (element.verifiableCredential)
          this.verifiableCredentials.push(element.verifiableCredential);
      });

      //service_title
      sovp!['credentialSubject']['gx:name'] = this.serviceDescription.service_title;

      //description
      sovp!['credentialSubject']['gx:description'] = formValue.description;

      sovp!['credentialSubject']['gx:termsAndConditions']['gx:URL'] = formValue.termsAndConditions;
      sovp!['credentialSubject']['gx:termsAndConditions']['gx:hash'] = this.termsAndConditionsHash;

      //location
      //sovp!['credentialSubject']['gx-service-offering:isAvailableOn'][0] = formValue.location.location_did;
    }

    this.serviceDescription.serviceOfferingVP = sovp;
    //this.serviceDescription.locatedServiceOfferingVP = lsovp
    return sovp;
  }

  checkDownloadVpValidity() {
    return (
      this.serviceDescription.service_title &&
      this.form.value.location.location_did
    );
  }

  private extractServiceTypes(formValue: ServiceDescription) {
    let formattedServiceTypes: any[] = [];
    formValue.service_type.forEach((t: any) => {
      formattedServiceTypes.push(t);
    });
    return formattedServiceTypes;
  }

  saveAsDraft() {
    this.form.value.status = this.statusEnum.DRAFT;
    // update service
    if (this.serviceDescription.id) {
        console.log('service draft to update')
        this.form.value.oldStatus = this.serviceDescription.status;
    } else {
        console.log('service draft to create')
    }
    this.save();
    this.openSnackBar(
      `Saved service ${this.serviceDescription.service_title} as draft`,
      'Hide',
      {
        duration: 5000,
        panelClass: ['green-snackbar'],
      }
    );
  }

  private save() {
    let formValue: ServiceDescription = this.form.value;
    this.serviceDescription.dataProduct = this.dataProductForm.value
    formValue.location.provider_designation = this.companyName;
    formValue.location.provider_did = `https://${this.companyName}.provider.demo23.gxfs.fr:participant:${this.participantId}/data.json`;

    this.serviceDescription.provider_did = formValue.location.provider_did;
    this.serviceDescription.location_did = formValue.location.location_did;

    this.serviceDescription.location = formValue.location;
    this.serviceDescription.locations = formValue.locations;
    (formValue.state = formValue.location.state);
      (formValue.urban_area = formValue.location.urban_area);
      (formValue.country_name = formValue.location.country_name);
      (formValue.state = this.serviceDescription.state);
    // remove whitespace
    this.serviceDescription.service_title = this.serviceDescription.service_title.trim();
    formValue.description = formValue.description?.trim();
    formValue.provider_designation = this.companyName;
    formValue.questionnaire = this.questionnaire;
    this.serviceDescription.questionnaire = this.questionnaire;
    if (!formValue.federations)
      formValue.federations = ['Gaia-X', 'ABC-federation'];
    this.serviceDescription.provider_designation = this.companyName;
    this.serviceDescription.urban_area = this.form.value.urban_area;
    this.serviceDescription.country_name = this.form.value.country_name;
    this.serviceDescription.state = this.form.value.state;
    this.serviceDescription.located_service_did = this.form.value.location_did;
    this.serviceDescription.provider_did = this.form.value.provider_did;
    formValue.dataProduct = this.dataProductForm.value;
    formValue.lastModified = new Date().toISOString();

    if (this.serviceDescription.serviceOfferingVP) {
        formValue.serviceOfferingVP = this.editSo();
    }

    if (this.serviceDescription.verifiablePresentation && [this.statusEnum.DRAFT, this.statusEnum.READY_TO_SIGN, undefined].includes(this.form.value.oldStatus)) {
        formValue.verifiablePresentation = this.serviceDescription.verifiablePresentation;
        // need to update SO in verifiableCredential
        const SOIndex: number = formValue.verifiablePresentation.verifiableCredential.findIndex((vc: any) => vc.credentialSubject?.type === 'gx:ServiceOffering');
        if (SOIndex !== -1) {
            formValue.verifiablePresentation.verifiableCredential[SOIndex] = formValue.serviceOfferingVP;
        }

        // need to change VC criteria
        const VcCreteriaIndex = formValue.verifiablePresentation.verifiableCredential.findIndex((vc: any) => vc.credentialSubject?.hasOwnProperty('aster-conformity:hasAssessedComplianceCriteria'));
        if (VcCreteriaIndex !== -1) {
            formValue.verifiablePresentation.verifiableCredential[VcCreteriaIndex]['credentialSubject']['aster-conformity:hasAssessedComplianceCriteria'] = this.getClaimsIds(this.questionnaire);
        }
    } else {
        formValue.verifiablePresentation = this.createVp();
        // Synchronize SO
        formValue.serviceOfferingVP = this.serviceDescription.serviceOfferingVP;
    }

    // if (!this.serviceDescription.id || this.form.value.oldStatus === undefined) {
    if (!this.serviceDescription.id || ![this.statusEnum.DRAFT, this.statusEnum.READY_TO_SIGN].includes(this.form.value.oldStatus)) {
      formValue.id = this.serviceDescription.verifiablePresentation['@id'];
      this.serviceDescription.id = formValue.id;
    } else {
      console.log('service id exist');
    }

    localStorage.removeItem('questionnaire');

    let storedLocations = localStorage.getItem('locations');
    let currentLocations = storedLocations ? JSON.parse(storedLocations) : [];

    if (
      !currentLocations.some(
        (item: any) =>
          item.country_name === this.serviceDescription.location.country_name &&
          item.state === this.serviceDescription.location.state &&
          item.urban_area === this.serviceDescription.location.urban_area
      )
    ) {
      currentLocations = currentLocations.concat(this.serviceDescription.location);
      localStorage.setItem('locations', JSON.stringify(currentLocations));
    }


    this.submitted.emit(formValue);
    this.router.navigateByUrl('/');
  }

  submit() {
    if (!this.serviceDescription.id) {
        this.form.value.verifiablePresentation = this.createVp();
        this.form.value.status = this.statusEnum.READY_TO_SIGN;
    } else {
        // initialize status
        this.form.value.status = this.serviceDescription.status;
        // generate vp for not ready to sign and draft service
        if ((this.serviceDescription.status !== this.statusEnum.READY_TO_SIGN) && (this.serviceDescription.status !== this.statusEnum.DRAFT)) {
            this.form.value.status = this.statusEnum.READY_TO_SIGN;
        } else if (this.serviceDescription.status === this.statusEnum.DRAFT) {
            // no need to generate vp just change status
            this.form.value.status = this.statusEnum.READY_TO_SIGN;
        }
        // initailize old status
        this.form.value.oldStatus = this.serviceDescription.status;
    }

    this.save();
    this.openSnackBar(
    `Submitted service ${this.serviceDescription.service_title}`,
    'Hide',
    {
        duration: 4000,
        panelClass: ['green-snackbar'],
    }
    );
    this.form.reset();
  }

  deleteFile(reference: ComplianceReferenceClaim) {
    reference.status = '';
    reference.verifiableCredential = '';
  }

  openSnackBar(message: string, action: string, type: any) {
    this._snackBar.open(message, action, type);
  }

  formatDate(date: Date): string {
    function pad(n: number): string {
      return n < 10 ? '0' + n : n.toString();
    }

    const yyyy = date.getFullYear();
    const MM = pad(date.getMonth() + 1);
    const dd = pad(date.getDate());
    const hh = pad(date.getHours());
    const mm = pad(date.getMinutes());
    const ss = pad(date.getSeconds());
    const ms = date.getMilliseconds();
    const timeZoneOffset = -date.getTimezoneOffset();
    const sign = timeZoneOffset >= 0 ? '+' : '-';
    const timeZone = pad(Math.abs(timeZoneOffset) / 60) + ':' + pad(Math.abs(timeZoneOffset) % 60);

    return `${yyyy}-${MM}-${dd}T${hh}:${mm}:${ss}.${ms}${sign}${timeZone}`;
  }

  uploadVP(file: FileSummary | Event) {
    const service = atob((file as FileSummary).base64);
    this.extractServiceData(JSON.parse(service));
  }

  extractServiceData(service: ServiceDescription) {
    service.verifiableCredential.forEach((vc: any) => {
        let credentialSubject = vc.credentialSubject;
        if (credentialSubject?.type === 'gx:ServiceOffering') {
            this.serviceDescription.service_title = credentialSubject['gx:name'];
            this.form.controls['service_title'].patchValue(
                credentialSubject['gx:name']
            );

            this.form.controls['description'].patchValue(
                credentialSubject['gx:description']
            );

            this.form.controls['termsAndConditions'].patchValue(
                credentialSubject['gx:termsAndConditions']['gx:URL']
            );

            this.form.controls['service_type'].patchValue(
                credentialSubject['gx:keyword']
            );

            let layer = credentialSubject['aster-conformity:layer'];
            if (Array.isArray(layer)) {
                this.form.controls['layer'].setValue(layer[0]);
            } else {
                this.form.controls['layer'].setValue(layer);
            }
        } else if (credentialSubject?.type === 'aster-conformity:Location') {
            let location = new ProviderLocation({
                location_did: credentialSubject['id'],
                country_name: credentialSubject['aster-conformity:country'],
                state: credentialSubject['aster-conformity:state'],
                urban_area: credentialSubject['aster-conformity:urbanArea'],
                provider_did: credentialSubject['aster-conformity:hasProvider'].id
            })
            this.form.controls['location'].patchValue(location);

            this.locations.push(location);
        } else if (credentialSubject && credentialSubject['aster-conformity:hasAssessedComplianceCriteria']) {
            credentialSubject['aster-conformity:hasAssessedComplianceCriteria'].forEach((claim: any) => {
                this.questionnaire.forEach((question: any, index: number) => {
                    if(question.id.includes(claim.id)) {
                        this.questionnaire[index].value = true;
                    }
                });
            })
        }
    });
  }
}
