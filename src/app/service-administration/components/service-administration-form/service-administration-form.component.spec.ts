import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceAdministrationFormComponent } from './service-administration-form.component';

describe('ServiceAdministrationFormComponent', () => {
  let component: ServiceAdministrationFormComponent;
  let fixture: ComponentFixture<ServiceAdministrationFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServiceAdministrationFormComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ServiceAdministrationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
