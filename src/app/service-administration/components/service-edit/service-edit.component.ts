import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceDescription } from '../../models/service-description';
import { ServiceAdministrationService } from '../../services/service-administration/service-administration.service';
import {ServiceStatusEnum} from '../../models/service-status-enum';

@Component({
  selector: 'app-service-edit',
  templateUrl: './service-edit.component.html',
  styleUrls: ['./service-edit.component.scss'],
})
export class ServiceEditComponent implements OnInit {
  constructor(
    private serviceAdministrationService: ServiceAdministrationService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  statusEnum: typeof ServiceStatusEnum = ServiceStatusEnum;
  editing: boolean = false;
  serviceDescription!: ServiceDescription;

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.serviceAdministrationService.collection$.subscribe((res) => {
        this.serviceDescription = res.find(
          (service) => service.id == params['id']
        )!;
        if (!this.serviceDescription) this.router.navigateByUrl('/');
      });
    });
  }

  save(serviceDescription: any) {
    this.editing = true;
    let oldStatus: string = serviceDescription.oldStatus;
    delete serviceDescription.oldStatus;
    if ((oldStatus === this.statusEnum.READY_TO_SIGN) || (oldStatus === this.statusEnum.DRAFT)) {
        this.serviceAdministrationService.updateServiceDescription(serviceDescription.id, serviceDescription).subscribe(() => {});
    } else {
        this.serviceAdministrationService.create(serviceDescription).subscribe(() => {});
    }
    this.editing = false;
  }
}
