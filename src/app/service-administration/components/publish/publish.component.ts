import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ServiceDescription } from '../../models/service-description';

export interface PublishDialogData {
  serviceDescription : ServiceDescription
}

@Component({
  selector: 'app-publish',
  templateUrl: './publish.component.html',
  styleUrls: ['./publish.component.scss']
})
export class PublishComponent implements OnInit {

  serviceDescription! : ServiceDescription;
  federationsToPush : string[] = [];
  form : FormGroup = new FormGroup({})

  constructor(
    public dialogRef: MatDialogRef<PublishComponent>,
    @Inject(MAT_DIALOG_DATA) public data: PublishDialogData,
  ) {
    this.serviceDescription = data.serviceDescription
    this.serviceDescription.federations?.forEach((f) => {
      if(f.toLowerCase() == 'gaia-x')
        this.form.addControl(f,new FormControl(true))
      else
        this.form.addControl(f,new FormControl(false))
    })

  }

  onClose(): void {
    this.dialogRef.close();
  }

  checkFederation(federation : string) {
    return federation.toLowerCase() == 'gaia-x'
  }

  ngOnInit(): void {

  }
  publish() {
    let formVal = Object.entries(this.form.value)
    let federations : string[] = [];
    formVal.forEach((f) => {
      if(f[1] == true)
        federations.push(f[0])
    })

    this.serviceDescription.federations = federations;
    this.dialogRef.close(this.serviceDescription);
  }

}
