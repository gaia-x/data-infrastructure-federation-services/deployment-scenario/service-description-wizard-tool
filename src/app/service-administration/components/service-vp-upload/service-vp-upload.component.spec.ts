import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceVpUploadComponent } from './service-vp-upload.component';

describe('ServiceVpUploadComponent', () => {
  let component: ServiceVpUploadComponent;
  let fixture: ComponentFixture<ServiceVpUploadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServiceVpUploadComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ServiceVpUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
