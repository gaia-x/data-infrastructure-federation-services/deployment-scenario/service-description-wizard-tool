import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FileSummary } from 'src/app/shared/file-upload/models/file-summary';

@Component({
  selector: 'app-service-vp-upload',
  templateUrl: './service-vp-upload.component.html',
  styleUrls: ['./service-vp-upload.component.scss'],
})
export class ServiceVpUploadComponent {
  private _file: FileSummary[] | Event | undefined;
  constructor(private router: Router) {}

  navigateBack() {
    this.router.navigateByUrl('/');
  }

  updateVP(file: FileSummary[] | Event) {
    this._file = file;
  }
}
