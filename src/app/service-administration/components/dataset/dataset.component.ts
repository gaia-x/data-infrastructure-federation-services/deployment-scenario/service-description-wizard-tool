import { Component, Inject, Injectable, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl, ValidationErrors, FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { PublishComponent } from '../publish/publish.component';
import { DistributionComponent } from '../distribution/distribution.component';
import * as shajs from 'sha.js';

@Component({
    selector: 'app-questionnaire',
    templateUrl: './dataset.component.html',
    styleUrls: ['./dataset.component.scss'],
})
@Injectable({
    providedIn: 'root',
})
export class DataSetComponent implements OnInit {
    dataSetForm!: FormGroup;
    form: FormGroup = new FormGroup({});
    questionnaire: any;
  name!: string;
    category!: string;

    minObsoleteDate: string | null = null;
    maxObsoleteDate: string | null = null;
    minExpirationDate: string | null = null;



    constructor(
        public dialog: MatDialog,
        private fb: FormBuilder,
        public dialogRef: MatDialogRef<PublishComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {

    }

    onDelete(): void {
        this.dialogRef.close("delete")
    }

    ngOnInit(): void {

        this.dataSetForm = this.fb.group(
            {
                copyrightOwnedBy: new FormControl('', [Validators.required]),
                personalDataPolicy: new FormControl(''),
                identifier: new FormControl(''),
                description: new FormControl(''),
                license: new FormControl('', [Validators.required]),
                obsoleteDateTime: [null],
                expirationDateTime: [null],
                title: new FormControl('', [Validators.required]),
                distributions: this.fb.array([], Validators.required)
            }, { validators: this.dateValidator }
        )
        if (this.data) {
            const distributionsArray = this.dataSetForm.controls['distributions'] as FormArray;
            this.dataSetForm.patchValue(this.data);
            this.data.distributions.forEach((distribution: any) => {
                const distributionFormGroup = this.fb.group({
                    identifier: [distribution.identifier],
                    description: [distribution.description],
                    obsoleteDateTime: [distribution.obsoleteDateTime],
                    expirationDateTime: [distribution.expirationDateTime],
                    title: [distribution.title],
                    algorithm: [distribution.algorithm],
                    fileType: [distribution.fileType],
                    byteSize: [distribution.byteSize],
                    fileLocation: [distribution.fileLocation],
                    fileHash: [distribution.fileHash],
                });
                distributionsArray.push(distributionFormGroup);
            });
        }
        this.initializeDateConstraints();
    }

    onEnterKey(event: Event): void {
        event.preventDefault();
    }

    hasError(controlName: string, errorName: string) {
        return this.dataSetForm.controls[controlName].hasError(errorName);
    }

    onConsentChange(event: any) {
        this.dataSetForm.value.consent = (event.checked ? 'true' : 'false');
    }

    dateValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
        if (!(control instanceof FormGroup)) {
            return null;
        }

        const issuanceDate = control.get('issuedDateTime');
        const obsoleteDate = control.get('obsoleteDateTime');
        const expirationDate = control.get('expirationDateTime');

        const issuanceDateObj = issuanceDate?.value ? new Date(issuanceDate.value) : null;
        const obsoleteDateObj = obsoleteDate?.value ? new Date(obsoleteDate.value) : null;
        const expirationDateObj = expirationDate?.value ? new Date(expirationDate.value) : null;

        if (issuanceDateObj && obsoleteDateObj) {
            const isObsoleteAfterIssuance = obsoleteDateObj.getTime() > issuanceDateObj.getTime();

            if (!isObsoleteAfterIssuance) {
                obsoleteDate?.setErrors({ obsoleteNotAfterIssuance: true });
            } else {
                obsoleteDate?.setErrors(null);
            }
        }

        if (issuanceDateObj && expirationDateObj) {
            const isIssuanceBeforeExpiration = issuanceDateObj.getTime() < expirationDateObj.getTime();

            if (!isIssuanceBeforeExpiration) {
                issuanceDate?.setErrors({ issuanceNotBeforeExpiration: true });
            } else {
                issuanceDate?.setErrors(null);
            }
        }

        if (expirationDateObj && obsoleteDateObj) {
            const isObsoleteBeforeExpiration = obsoleteDateObj.getTime() < expirationDateObj.getTime();

            if (!isObsoleteBeforeExpiration) {
                obsoleteDate?.setErrors({ obsoleteNotBeforeExpiration: true });
            } else {
                obsoleteDate?.setErrors(null);
            }
        }

        // this.initializeDateConstraints();

        return null;
    };


    initializeDateConstraints() {
        const today = new Date();
        const tomorrow = new Date(today);
        tomorrow.setDate(tomorrow.getDate() + 1);
        this.minObsoleteDate = tomorrow.toISOString().split('T')[0];

        this.dataSetForm.get('obsoleteDateTime')?.valueChanges.subscribe((obsoleteDate: Date | null) => {
            if (obsoleteDate) {
                const adjustedObsoleteDate = new Date(obsoleteDate);
                adjustedObsoleteDate.setDate(adjustedObsoleteDate.getDate() + 2);
                this.minExpirationDate = adjustedObsoleteDate.toISOString().split('T')[0];

                const expirationDateControl = this.dataSetForm.get('expirationDateTime');
                const currentExpirationDate = expirationDateControl?.value;

                if (currentExpirationDate && new Date(currentExpirationDate) <= new Date(obsoleteDate)) {
                    expirationDateControl?.setValue(null);
                }
            } else {
                this.minExpirationDate = null;
            }
        });
    }


    onDateChange(): void {
        const issuanceDate = this.dataSetForm.get('issuedDateTime');
        const obsoleteDate = this.dataSetForm.get('obsoleteDateTime');
        const expirationDate = this.dataSetForm.get('expirationDateTime');
        if (issuanceDate && obsoleteDate && expirationDate) {
            this.dataSetForm.updateValueAndValidity();
        }
    }



    domains: string[] = [];

    datasetHashes: string[] = [];

    addCopyrightOwner(inputRef: HTMLInputElement): void {

        if (inputRef.value !== '') {
            const copyrightOwner = this.dataSetForm.get('copyrightOwnedBy') as FormControl;
            const currentValue = copyrightOwner.value as string[];
            const newValue = [...currentValue, inputRef.value];
            copyrightOwner.setValue(newValue);
            inputRef.value = '';
        }
    }

    removeCopyrightOwner(index: number): void {
        const copyrightOwner = this.dataSetForm.get('copyrightOwnedBy') as FormControl;
        const currentValue = copyrightOwner.value as string[];
        const newValue = currentValue.filter((_, i) => i !== index);
        copyrightOwner.setValue(newValue);
    }

    addLicense(inputRef: HTMLInputElement): void {
        if (inputRef.value !== '') {
            const licenses = this.dataSetForm.get('license') as FormControl;
            const currentValue = licenses.value as string[];
            const newValue = [...currentValue, inputRef.value];
            licenses.setValue(newValue);
            inputRef.value = '';
        }
    }

    removeLicense(index: number): void {
        const licenses = this.dataSetForm.get('license') as FormControl;
        const currentValue = licenses.value as string[];
        const newValue = currentValue.filter((_, i) => i !== index);
        licenses.setValue(newValue);
    }

    onSubmit() {
        console.log(this.dataSetForm.value.distributions)
        if ((this.dataSetForm.valid) && this.dataSetForm.value.copyrightOwnedBy.length > 0 && this.dataSetForm.value.license.length > 0) {
            this.dialogRef.close(this.dataSetForm);
            this.form.reset();
        }
    }

    onClose() {
        this.dialogRef.close()
    }


    openDistributionForm(values?: any) {

        const dialogRef = this.dialog.open(DistributionComponent, {
            data: values,
            width:'30vw',
            height:'64vh',
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                console.log(result)
                if (result === "delete") {
                    this.removeDistribution(values);
                    // console.log(values)
                    // const foundIndex = this.dataProductForm.value.aggregationof.findIndex((item: any) => item.hash === values.hash);
                    // if (foundIndex !== -1) {
                    //   this.dataProductForm.value.aggregationof = this.dataProductForm.value.aggregationof.filter((item: any) => item.hash !== values.hash);
                    //   console.log("OOOK", this.dataProductForm.value.aggregationOf)
                }
                else if (result) {
                    this.addDistribution(result);
                }
            }
        })
    }

    createId(val: string) {
        return shajs('sha256').update(val).digest('hex');
    }

    get distributions(): FormArray {
        return this.dataSetForm.get('distributions') as FormArray;
    }

    addDistribution(form: any) {
        const values = form.value;
        if (values.identifier === "") {
            const generatedUuid = this.createId(values.title);
            const dataSet = { ...values };
            dataSet.identifier = generatedUuid;
            this.datasetHashes.push(generatedUuid);
            this.distributions.push(this.fb.control(dataSet));
        } else {
            const identifierToUpdate = values.identifier;
            const foundIndex = this.distributions.value.findIndex((element: any) => element.identifier === identifierToUpdate);
            if (foundIndex !== -1) {
                const updatedObject = { ...this.distributions.value[foundIndex], ...values };
                this.distributions.at(foundIndex).setValue(updatedObject);
            }
        }
    }

    removeDistribution(values: any) {
        const foundIndex = this.distributions.value.findIndex((item: any) => item.identifier === values.identifier);
        if (foundIndex !== -1) {
            this.distributions.removeAt(foundIndex);
        }
    }

}
