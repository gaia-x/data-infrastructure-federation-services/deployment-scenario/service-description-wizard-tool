import { Component, Inject, Injectable, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PublishComponent } from '../publish/publish.component';

@Component({
  selector: 'app-questionnaire',
  templateUrl: './questionnaire.component.html',
  styleUrls: ['./questionnaire.component.scss'],
})
@Injectable({
  providedIn: 'root',
})
export class QuestionnaireComponent implements OnInit {
  form: FormGroup = new FormGroup({});
  questionnaire: any;
  name!: string;
  category!: string;

  constructor(
    public dialogRef: MatDialogRef<PublishComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.questionnaire = data
    .map((d: any) => ({ ...d, value: d.value === '' ? false : d.value}))
    .reduce((accumulator: any, currentObject: any) => {
        const existingObject = accumulator.find(
          (obj: any) => obj.name === currentObject.name
        );
        if (existingObject) {
          existingObject.id += `, ${currentObject.id}`;
        } else {
          accumulator.push(currentObject);
        }
        return accumulator;
      }, [])
      .sort((a: any, b: any) => {
        if (a.name > b.name) {
          return 1;
        }
        if (a.name < b.name) {
          return -1;
        }
        return 0;
      });
  }

  onClose(): void {
    localStorage.setItem('questionnaire', this.data);
    this.dialogRef.close();
  }

  ngOnInit(): void {}

  isFilled() {}

  filterQuestionsBy(category: string) {
    return this.questionnaire.filter(
      (q: { category: string }) => q.category == category
    );
  }

  changeContractualGovernance() {
    this.filterQuestionsBy('Contractual governance').filter(
      (q: { value: any }) => q.value
    );
  }

  changeTransparency() {
    this.filterQuestionsBy('Transparency').filter(
      (q: { value: any }) => q.value
    );
  }

  changeEuropeanControl() {
    this.filterQuestionsBy('European Control').filter(
      (q: { value: any }) => q.value
    );
  }

  submit() {
    localStorage.setItem('questionnaire', JSON.stringify(this.questionnaire));
    this.form.reset();
    return this.data;
  }
}
