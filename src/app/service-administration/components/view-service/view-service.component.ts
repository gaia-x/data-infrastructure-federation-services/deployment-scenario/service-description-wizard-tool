import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigService } from 'src/app/core/services/config.service';
import { ServiceDescription } from '../../models/service-description';
import { ServiceStatusEnum } from '../../models/service-status-enum';
import { ServiceAdministrationService } from '../../services/service-administration/service-administration.service';

@Component({
  selector: 'app-view-service',
  templateUrl: './view-service.component.html',
  styleUrls: ['./view-service.component.scss'],
})
export class ViewServiceComponent implements OnInit {
  @Input()
  serviceDescription: ServiceDescription = new ServiceDescription();
  @Input()
  drawerView: any;
  claimCategories: string[] = [];
  companyName: string = ''
  constructor(
    private router: Router,
    private configService: ConfigService,
    private serviceAdministrationService: ServiceAdministrationService,
  ) {
    this.companyName = configService.getCompanyName();

  }

  ngOnInit(): void {

    this.serviceAdministrationService.claimsReferences$.subscribe(() => {
    });


  }

  getServiceTypes() {

    return (this.serviceDescription.service_type).toString().replaceAll("'", "").replaceAll("[", "").replaceAll("]", "").split(",");
  }

  getComplianceReferenceTitles() {
    return (this.serviceDescription.compliance_reference_title.toString().replaceAll("'", "").replaceAll("[", "").replaceAll("]", "").split(","));
  }
  getLayer() {
    return this.serviceDescription.layer.replaceAll("'", "").replaceAll("[", "").replaceAll("]", "");
  }
  goToEditPage(serviceDescription: ServiceDescription) {
    this.router.navigateByUrl(`/${serviceDescription.id}`);
  }

  toNumber(string: string) {
    return Number(string);
  }

  isPublished() {
    return this.serviceDescription.status == ServiceStatusEnum.PUBLISHED;
  }

  filterClaimsBy(category: string) {
    if (this.serviceDescription.complianceReferenceClaims) {
      return this.serviceDescription.complianceReferenceClaims.filter(
        (p) => p.type == category
      );
    }
    return null;
  }

  close() {
    this.drawerView.close();
  }

  seeVC(service: ServiceDescription) {

    let serivceVP = service.verifiablePresentation;
    const blob = new Blob([JSON.stringify(serivceVP, null, 2)], {
        type: 'text/json',
      });
    
    let fileURL = URL.createObjectURL(blob);
    let win = window.open();
    win?.document.write('<iframe src="' + fileURL + '" frameborder="0" style="border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;" allowfullscreen></iframe>')
  }
}
