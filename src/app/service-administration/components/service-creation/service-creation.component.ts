import { Component, OnInit } from '@angular/core';
import { ServiceDescription } from '../../models/service-description';
import { ServiceAdministrationService } from '../../services/service-administration/service-administration.service';


@Component({
  selector: 'app-service-creation',
  templateUrl: './service-creation.component.html',
  styleUrls: ['./service-creation.component.scss']
})
export class ServiceCreationComponent implements OnInit {

  serviceDescription : ServiceDescription = new ServiceDescription();
  creating = false;

  constructor(private serviceAdministrationService : ServiceAdministrationService) { }

  ngOnInit(): void {
  }

  save(serviceDescription : ServiceDescription) {
    this.creating = true;
    this.serviceAdministrationService.create(serviceDescription).subscribe(() => this.creating = false)
  }

}
