import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ServiceAdministrationComponent } from './components/service-administration/service-administration.component';
import { ServiceCreationComponent } from './components/service-creation/service-creation.component';
import { ServiceEditComponent } from './components/service-edit/service-edit.component';
import { ServiceVpUploadComponent } from './components/service-vp-upload/service-vp-upload.component';
import { SignClaimsComponent } from './components/sign-claims/sign-claims.component';

const routes: Routes = [
  { path: '', component: ServiceAdministrationComponent },
  { path: 'create', component: ServiceCreationComponent },
  { path: ':id', component: ServiceEditComponent },
  { path: ':id/add-reference', component: ServiceVpUploadComponent },
  { path: ':id/sign-claims', component: SignClaimsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceAdministrationRoutingModule { }
