import { ServiceDescription } from "../../models/service-description";

export interface IServiceAdministrationService {

    get(): void;

    delete() : void;

    create(serviceDescription : ServiceDescription) : void;
}
