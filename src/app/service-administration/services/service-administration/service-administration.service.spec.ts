import { TestBed } from '@angular/core/testing';

import { ServiceAdministrationService } from './service-administration.service';

describe('ServiceAdministrationService', () => {
  let service: ServiceAdministrationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceAdministrationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
