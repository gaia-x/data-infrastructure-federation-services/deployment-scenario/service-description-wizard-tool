import { Injectable, OnInit } from '@angular/core';

import { BehaviorSubject, map, Observable, tap } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { plainToInstance } from 'class-transformer';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ServiceDescription } from 'src/app/service-administration/models/service-description';
import { environment } from 'src/environments/environment';
import { ComplianceReferenceClaim } from 'src/app/service-administration/models/compliance-reference-claim';
import { ProviderLocation } from 'src/app/service-administration/models/provider-location';
import { ConfigService } from 'src/app/core/services/config.service';
import { ProviderDesignation } from 'src/app/service-administration/models/provider-designation';
import * as shajs from 'sha.js';
import { ServiceStatusEnum } from '../../models/service-status-enum';

@Injectable({
  providedIn: 'root',
})
export class ServiceAdministrationService implements OnInit {
  collection$!: BehaviorSubject<ServiceDescription[]>;

  private urlApi: string = `https://wizard-tool.${environment.PROVIDER}.provider.demo23.gxfs.fr/api/services`;
  private service_typeUrl: string = `${environment.CATALOGUE_URL}/api/service_types`;
  private serviceLayersUrl: string = `${environment.CATALOGUE_URL}/api/layers`;
  private federationsUrl: string = `https://wizard-tool.${environment.PROVIDER}.provider.demo23.gxfs.fr/api/federations`;
  private referencesUrl: string = `${environment.CATALOGUE_URL}/api/compliance_references`;
  private locationsUrl: string = `${environment.CATALOGUE_URL}/api/locations`;
  private providersUrl: string = `${environment.CATALOGUE_URL}/api/providers`;
  private questionnaireUrl: string = `${environment.CATALOGUE_URL}/api/compliance_criterions?canBeSelfAssessed=true`;
  private serviceOfferingsUrl: string = `https://wizard-tool.${environment.PROVIDER}.provider.demo23.gxfs.fr/api/serviceOfferings`;
  private locatedServiceOfferingsUrl: string = `https://wizard-tool.${environment.PROVIDER}.provider.demo23.gxfs.fr/api/locatedServiceOfferings`;

  serviceLayers$!: BehaviorSubject<string[]>;
  service_type$!: BehaviorSubject<string[]>;
  federations$!: BehaviorSubject<string[]>;
  claimsReferences$!: BehaviorSubject<ComplianceReferenceClaim[]>;
  locations$!: BehaviorSubject<ProviderLocation[]>;
  questionnaire$!: BehaviorSubject<any>;
  serviceOfferings$!: BehaviorSubject<string[]>;
  providers$ = new BehaviorSubject<string[]>([]);
  locatedServiceOfferings$!: BehaviorSubject<string[]>;
  providerName: string = '';
  serviceApiUrl: string = '';
  loading: BehaviorSubject<boolean>;
  compliance_reference_title: string = '';
  statusEnum: typeof ServiceStatusEnum = ServiceStatusEnum;

  constructor(private httpClient: HttpClient, private config: ConfigService) {
    this.collection$ = new BehaviorSubject<ServiceDescription[]>([]);
    this.serviceLayers$ = new BehaviorSubject<string[]>([]);
    this.service_type$ = new BehaviorSubject<string[]>([]);
    this.federations$ = new BehaviorSubject<string[]>([]);
    this.providers$ = new BehaviorSubject<string[]>([]);
    this.claimsReferences$ = new BehaviorSubject<ComplianceReferenceClaim[]>(
      []
    );
    this.locations$ = new BehaviorSubject<ProviderLocation[]>([]);
    this.questionnaire$ = new BehaviorSubject([]);
    this.serviceOfferings$ = new BehaviorSubject<string[]>([]);
    this.locatedServiceOfferings$ = new BehaviorSubject<string[]>([]);
    this.loading = new BehaviorSubject(false);

    this.config.load().then(() => {
      let providerName = environment.PROVIDER;
      localStorage.setItem("logged", providerName);
      this.serviceApiUrl = 'https://' + this.config.getDid().split(':')[2] + '/participant/' + shajs('sha256').update(providerName).digest('hex');
      this.urlApi = `https://wizard-tool.${providerName}.provider.demo23.gxfs.fr/api/services`;
      this.compliance_reference_title;
      this.refreshLayers();
      this.getCompanyName();
      this.refreshReferences();
      this.refreshQuestionnaire();
      this.refreshFederations();
      this.refreshLocations();
      this.refreshTypes();
      this.refreshCollection();
      this.refreshServiceOfferings();
      this.refreshLocatedServiceOfferings();
    });
  }

  ngOnInit() { }

  getLoading() {
    return this.loading;
  }

  public refreshCollection(): void {
    this.loading.next(true);
    this.httpClient
      .get<ServiceDescription[]>(this.urlApi)
      .pipe(
        map((res: ServiceDescription[]) =>
          plainToInstance(ServiceDescription, res).filter(
            (data) =>
              data.provider_designation.toLowerCase() === environment.PROVIDER.toLowerCase()
          )
        )
      )
      .subscribe(async (data) => {
        if (data.length == 0) {
          await this.populate();
        } else {
          this.collection$.next(data);
        }
        this.loading.next(false);
      });
  }

  public async populate() {
    let lsofurl = 'https://' + this.config.getDid().split(':')[2] + '/api/get_objects/located-service-offering';
    // let lsofurl = this.serviceApiUrl + '/located-service-offering/index.json';
    this.httpClient.get<string[]>(lsofurl).subscribe(async (data: any) => {
      let serviceDescriptions: ServiceDescription[] = [];
    });
  }

  public refreshQuestionnaire(): void {
    this.httpClient.get<any>(this.questionnaireUrl).subscribe((data) => {
      //console.log(data)
      const tab: object[] = [];
      data.forEach(function (item: { [x: string]: { [x: string]: any } }) {
        let critName = item['aster-conformity:hasName'];
        const critCategory = item['aster-conformity:hasCategory'];
        const critDescription = item['aster-conformity:hasDescription'];
        const critID = item['id'];
        tab.push({
          name: critName,
          description: critDescription,
          category: critCategory,
          value: '',
          id: critID,
        });
      });
      let uniqueArray = [
        ...new Set(tab.map((item) => JSON.stringify(item))),
      ].map((item) => JSON.parse(item));
      uniqueArray.sort(function (a, b) {
        return parseInt(a.name.split(' ')[1]) - parseInt(b.name.split(' ')[1]);
      });
      //console.log(uniqueArray);
      this.questionnaire$.next(uniqueArray);
    });
  }

  public async refreshLocations(): Promise<void> {
    this.providerName = environment.PROVIDER;
    const cachedLocations = localStorage.getItem('locations');
    if (cachedLocations) {
      const locations = plainToInstance(
        ProviderLocation,
        JSON.parse(cachedLocations)
      );
      this.locations$.next(locations);
    }
    // this.httpClient
    //   .get<ProviderLocation[]>(this.locationsUrl)
    //   .pipe(
    //     map((res: any[]) =>
    //         res.filter((l) => {
    //                 return l['@id']
    //                     .toLowerCase()
    //                     .includes(this.providerName.toLowerCase())
    //             }
    //         )
    //     )
    //   )
    //   .subscribe((data) => {
    //     const transformData = data.map((loc) => {
    //         return new ProviderLocation({
    //             '@id': loc['@id'],
    //             'country_name': loc['credentialSubject']['aster-conformity:country'],
    //             'state': loc['credentialSubject']['aster-conformity:state'],
    //             'urban_area': loc['credentialSubject']['aster-conformity:urbanArea'],
    //             'location_did': loc['@id'],
    //             'provider_did': loc['credentialSubject']['aster-conformity:hasProvider'].id,
    //             'provider_designation': loc['credentialSubject']['aster-conformity:providerDesignation']
    //         })
    //     });
    //
    //     this.locations$.next(transformData);
    //     localStorage.setItem('locations', JSON.stringify(transformData));
    //   });
  }



  public refreshLayers(): void {
    this.httpClient.get<string[]>(this.serviceLayersUrl).subscribe((data) => {
      this.serviceLayers$.next(data);
    });
  }

  public refreshFederations(): void {
    this.httpClient.get<string[]>(this.federationsUrl).subscribe((data) => {
      this.federations$.next(data);
    });
  }

  public refreshTypes(): void {
    this.httpClient.get<string[]>(this.service_typeUrl).subscribe((data) => {
      this.service_type$.next(data);
    });
  }

  public getCompanyName() {
    return this.httpClient
      .get<ProviderDesignation[]>(this.providersUrl)
      .pipe(
        map((res) =>
          res.find((p) => p.provider_did.includes(this.config.getDid()))
        )
      );
  }

  getCritFromApi(): Observable<any> {
    return this.httpClient.get<any>(this.questionnaireUrl);
  }

  public refreshReferences(): void {
    this.httpClient
      .get<ComplianceReferenceClaim[]>(this.referencesUrl)
      .pipe(
        map((res: ComplianceReferenceClaim[]) =>
          plainToInstance(ComplianceReferenceClaim, res)
        )
      )
      .subscribe((data) => {
        this.claimsReferences$.next(data);
      });
  }

  public delete(id: string): Observable<string> {
    return this.httpClient
      .delete<string>(`${this.urlApi}/${id}`)
      .pipe(tap(() => this.refreshCollection()));
  }

  public get(name: string): Observable<ServiceDescription> {
    return this.httpClient
      .get<ServiceDescription>(`${this.urlApi}/${name}`)
      .pipe(
        map((res: ServiceDescription) =>
          plainToInstance(ServiceDescription, res)
        )
      );
  }

  public updateServiceDescription(
    id: string,
    serviceDescription: ServiceDescription
  ): Observable<null> {
    return this.httpClient.put<null>(`${this.urlApi}/${id}`, serviceDescription).pipe(tap(() => this.refreshCollection()));
  }

  public pushToProviderCatalogue(
    name: string,
    serviceDescription: ServiceDescription
  ): Observable<null> {
    const apiKey = localStorage.getItem("apiKeyCatalogue") || "";
    const headers = new HttpHeaders().set('x-api-key', apiKey);
    this.providerName = environment.PROVIDER;
    return this.httpClient
      .put<null>(`${this.urlApi}/${name}`, serviceDescription)
      .pipe(
        concatMap(() => this.httpClient.post<null>(`https://catalogue-api.${this.providerName}.demo23.gxfs.fr/catalog/items/
        `, serviceDescription.verifiablePresentation.verifiableCredential[0].id || serviceDescription.verifiablePresentation.verifiableCredential[0]["@id"], { headers })),
        tap(() => this.refreshCollection()));
  }

  public create(serviceDescription: ServiceDescription): Observable<null> {
    return this.httpClient
      .post<null>(`${this.urlApi}`, serviceDescription)
      .pipe(tap(() => this.refreshCollection()));
  }

  public duplicateService(serviceDescription: ServiceDescription): Observable<null> {
    let newService = this.duplicate(serviceDescription);
    return this.httpClient
        .post<null>(`${this.urlApi}`, newService)
        .pipe(tap(() => this.refreshCollection()));
  }

  public refreshLocatedServiceOfferings(): void {
    this.httpClient
      .get<string[]>(this.locatedServiceOfferingsUrl)
      .subscribe(async (data: string[]) => {
        this.locatedServiceOfferings$.next(data);
      });
  }

  public post(serviceDescription: ServiceDescription) {
    return this.httpClient
      .post(`${this.urlApi}/${name}`, serviceDescription)
      .pipe(tap(() => this.refreshCollection()));
  }

  public refreshServiceOfferings(): void {
    this.httpClient
      .get<string[]>(this.serviceOfferingsUrl)
      .subscribe((data: string[]) => {
        this.serviceOfferings$.next(data);
      });
  }

  public async toServiceDescription(vp: any) {
    let sd: ServiceDescription = new ServiceDescription();
    this.httpClient
      .get(this.didToUrl(sd.located_service_did))
      .subscribe((res) => (sd.locatedServiceOfferingVP = res));
    let so: any = '';
    await this.getServiceOffering(vp).then((res) => (so = res));

    sd.serviceOfferingVP = so;
    sd.id = vp['id'].split(':')[4].split('/')[2];
    if (so['credentialSubject']) {
      sd.description = so['credentialSubject']['dct:description'];
      sd.layer = so['credentialSubject']['gax-service:layer']['@value'];
      sd.service_type = [];
      sd.service_title =
        so['credentialSubject']['gax-service:serviceTitle']['@value'];
      so['credentialSubject']['gax-service:serviceType'].forEach((t: any) =>
        sd.service_type.concat(t['@value'])
      );
    } else {
      sd.description = so['dct:description'];
      sd.layer = so['gax-service:layer']['@value'];
      sd.service_type = [];
      sd.service_title = so['gax-service:serviceTitle']['@value'];
      so['gax-service:serviceType'].forEach((t: any) =>
        sd.service_type.push(t['@value'])
      );
    }

    sd.federations = ['Gaia-X'];
    sd.federations.push('ABC-federation');
    let loc: any = await this.getLocation(so);
    (sd.location_did = vp['gax-service:isHostedOn']['@id']);
      (sd.country_name = loc['gax-participant:country']['@value']);
      (sd.state = loc['gax-participant:state']['@value']);
      (sd.urban_area = loc['gax-participant:urbanArea']['@value']);
      (sd.provider_did = vp['id']);
      (sd.lastModified = new Date().toISOString());

    return sd;
  }

  async getServiceOffering(vp: any) {
    let response = '';
    let url = this.didToUrl(
      vp['@id'].toString()
    );
    await this.httpClient
      .get(url)
      .toPromise()
      .then((res: any) => (response = res));
    return response;
  }

  async getLocation(so: any) {
    let locurl;
    if (so['credentialSubject'])
      locurl = this.didToUrl(
        so['credentialSubject']['gax-service:isAvailableOn'][0][
          '@id'
        ].toString()
      );
    else {
      locurl = this.didToUrl(
        so['gax-service:isAvailableOn'][0]['@id'].toString()
      );
    }
    let loc: string = '';
    await this.httpClient
      .get<string>(locurl)
      .toPromise()
      .then((res) => (loc = res!));
    return loc;
  }

  didToUrl(did: string) {
    let d = did.split(':');
    return 'https://' + d[2] + '/' + d[3] + '/' + d[4];
  }

  postToNotarization(request: any): Observable<any> {
    return this.httpClient.post(
      `${environment.NOTARIZATION_URL}/api/v2210/create-vc`,
      JSON.stringify(request),
      {
        headers: {
          'Content-Type': 'application/json',
        },
      }
    );
  }

  postToNotarizationForCertificateClaimVC(request: any): Observable<any> {
    return this.httpClient.post(
      `${environment.NOTARIZATION_URL}/api/create-third-party-compliance-certificate-credential`,
      JSON.stringify(request),
      {
        headers: {
          'Content-Type': 'application/json',
        },
      }
    );
  }
  postToNotarizationForVP(request: any): Observable<any> {
    return this.httpClient.post(
      `${environment.NOTARIZATION_URL}/api/v2210/create-vp`,
      JSON.stringify(request),
      {
        headers: {
          'Content-Type': 'application/json',
        },
      }
    );
  }

  postToCompliance(request: any) {
    let fileContent = JSON.stringify(request);

    return this.httpClient.post(`${environment.COMPLIANCE_URL}/api/credential-offers`, fileContent)
  }

  postToLabelling(request: any) {
    let fileContent = JSON.stringify(request);

    return this.httpClient.post(environment.LABELLING_URL, fileContent, {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
    });
  }

  postToStoreVP(request: any) {
    let content = JSON.stringify({"objectjson": request});
    let storeFileUrl = 'https://' + this.config.getDid().split(':')[2] + '/api/store_object/vp';
    const apiKey = localStorage.getItem("apiKeyUserAgent") || ""

    return this.httpClient.post(storeFileUrl, content, {
      headers: new HttpHeaders().set('x-api-key', apiKey).append('Content-Type', 'application/json'),
    });
  }

  postToStoreVC(request: any) {
    let content = JSON.stringify({"objectjson": request});
    let storeFileUrl = 'https://' + this.config.getDid().split(':')[2] + '/api/store_object/vc'

    const apiKey = localStorage.getItem("apiKeyUserAgent") || ""
    return this.httpClient.post(storeFileUrl, content, { headers: new HttpHeaders().set("x-api-key", apiKey).append('Content-Type', 'application/json') })
  }

  createId(val: string) {
    return shajs('sha256').update(val).digest('hex');
  }

  formatDate(date: Date): string {
    function pad(n: number): string {
      return n < 10 ? '0' + n : n.toString();
    }

    const yyyy = date.getFullYear();
    const MM = pad(date.getMonth() + 1);
    const dd = pad(date.getDate());
    const hh = pad(date.getHours());
    const mm = pad(date.getMinutes());
    const ss = pad(date.getSeconds());
    const ms = date.getMilliseconds();
    const timeZoneOffset = -date.getTimezoneOffset();
    const sign = timeZoneOffset >= 0 ? '+' : '-';
    const timeZone = pad(Math.abs(timeZoneOffset) / 60) + ':' + pad(Math.abs(timeZoneOffset) % 60);

    return `${yyyy}-${MM}-${dd}T${hh}:${mm}:${ss}.${ms}${sign}${timeZone}`;
  }

  duplicate(service: ServiceDescription) {
    let participantId = this.createId(service.provider_designation);
    let soid = this.createId((Math.random() + 1).toString(36).substring(7));
    let id = `https://${service.provider_designation}.provider.demo23.gxfs.fr:participant:${participantId}/vc/${soid}/data.json`;
    let vpid = this.createId((Math.random() + 1).toString(36).substring(7));
    let sacc = this.createId((Math.random() + 1).toString(36).substring(7));
    let vcsToDelete: any = [];
    let duplicatedService: any;

    duplicatedService = service;
    delete duplicatedService.id;
    duplicatedService.compliance_label_level = 0;
    if (duplicatedService.status !== this.statusEnum.DRAFT) {
        duplicatedService.status = this.statusEnum.READY_TO_SIGN;
    }
    duplicatedService.serviceOfferingVP.id = id;
    duplicatedService.serviceOfferingVP.credentialSubject.id = id;
    duplicatedService.lastModified = new Date().toISOString();
    duplicatedService.verifiablePresentation['@id'] = `https://${service.provider_designation}.provider.demo23.gxfs.fr:participant:${participantId}/vp/${vpid}/data.json`;
    duplicatedService.verifiablePresentation.verifiableCredential.forEach((vc: any, index: number) => {
        let vcType = vc.credentialSubject.type;
        if (vcType === 'gx:ServiceOffering') {
            if (duplicatedService.verifiablePresentation.verifiableCredential[index]['@id']) {
                delete duplicatedService.verifiablePresentation.verifiableCredential[index]['@id'];
            }
            duplicatedService.verifiablePresentation.verifiableCredential[index].id = id;
            duplicatedService.verifiablePresentation.verifiableCredential[index].credentialSubject.id = id;
        } else if (vc.type === 'SelfAssessedComplianceCriteriaClaim' || vc.credentialSubject.hasOwnProperty('aster-conformity:hasAssessedComplianceCriteria')) {
            if (duplicatedService.verifiablePresentation.verifiableCredential[index]['@id']) {
                delete duplicatedService.verifiablePresentation.verifiableCredential[index]['@id'];
            }
            duplicatedService.verifiablePresentation.verifiableCredential[index].id = `https://${service.provider_designation}.provider.demo23.gxfs.fr:participant:${participantId}/self-assessed-compliance-criteria-claim/${sacc}/data.json`;
            duplicatedService.verifiablePresentation.verifiableCredential[index]['credentialSubject']['aster-conformity:hasServiceOffering'] = id;
        }

        if (!['gx:LegalParticipant', 'gx:GaiaXTermsAndConditions', 'gx:legalRegistrationNumber'].includes(vcType)) {
            if (vc.proof) {
                delete vc.proof;
            }
        }

        duplicatedService.verifiablePresentation.verifiableCredential = duplicatedService.verifiablePresentation.verifiableCredential.filter((vc: any) => vc.credentialSubject.type !== 'aster-conformity:grantedLabel' && !Array.isArray(vc.credentialSubject) && !vc.credentialSubject.hasOwnProperty('aster-conformity:hasComplianceCertificationScheme'));
    })

    if (duplicatedService.verifiablePresentation.proof) {
        delete duplicatedService.verifiablePresentation.proof;
    }

    return duplicatedService;
  }
}
