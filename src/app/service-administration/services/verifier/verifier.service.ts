import {HttpClient} from "@angular/common/http";
import {Observable, of, timeout} from "rxjs";
import {catchError, map} from "rxjs/operators";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class VerifierService {

  constructor(private http: HttpClient) {}

  verifyVCs(vcs: string): Observable<Boolean>{
    console.log(vcs)
    return this.http.post("https://verifier.aster-x.demo23.gxfs.fr/api/rules", vcs, { headers : {"Content-Type": "application/json"} })
      .pipe(
        timeout(5000),
        map(() => true),
        catchError(error => {
          if (error.name === 'HttpErrorResponse' && error.status === 0) {
            console.error(error);
          }
          return of(false);
        })
      );
  }
}
