import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import { ResolverService } from './resolver.service';
import {Observable, of, timeout} from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import * as validator from 'validator';

export function urlValidator(service: ResolverService): ValidatorFn {
  return (control: AbstractControl): Observable<ValidationErrors | null> => {
    const url = control.value;

    if (validator.default.isURL(url)) {
      return service.checkUrl(url)
        .pipe(
          timeout(5000),
          map(isResolvable => isResolvable ? null : { unresolvableUrl: true }),
          catchError(() => of({ unresolvableUrl: true }))
        );
    } else {
      return of({ invalidUrl: true });
    }

  };
}
