import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable, of, timeout} from "rxjs";
import {catchError, map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ResolverService {
  constructor(private http: HttpClient) {}

  checkUrl(url: string): Observable<boolean> {
    return this.http.get(url, { responseType: 'text' })
      .pipe(
        timeout(5000),
        map(() => true),
        catchError(error => {
          if (error.name === 'HttpErrorResponse' && error.status === 0) {
            console.error(error);
          }
          return of(false);
        })
      );
  }

  checkUrlContent(url: string): Observable<string> {
    return this.http.get(url, { responseType: 'text' })
      .pipe(
        timeout(5000),
        catchError(err => {
          console.error("Error fetching URL content:", err);
          return of("");
        })
      );
  }
}
