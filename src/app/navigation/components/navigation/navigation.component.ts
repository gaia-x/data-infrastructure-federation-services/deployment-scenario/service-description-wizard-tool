import { Component, OnInit} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfigService } from 'src/app/core/services/config.service';
import { DisclaimerDialogComponent } from '../disclaimer-dialog/disclaimer-dialog.component';
import { Router } from '@angular/router';
import { KeycloakService } from 'keycloak-angular';
import { CookieService } from 'ngx-cookie-service';
import { environment } from 'src/environments/environment';
import { ApiKeysModalComponent } from '../api-keys-modal/api-keys-modal.component';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent implements OnInit {
  companyName: string = '';
  logged: boolean = false;
  decodeToken: any;
  isDropdownOpen = false;
  constructor(
    private config: ConfigService,
    private dialog: MatDialog,
    private router: Router,
    private keycloakService: KeycloakService,
    private cookiesService: CookieService,
  ) {
    this.companyName = this.config.getCompanyName();
    this.decodeToken =  JSON.parse(localStorage.getItem("decodedVP")!);
  }

  ngOnInit(): void {
    this.logged = !this.keycloakService.isTokenExpired();
    setTimeout(() => {
        this.openDialog();
        this.decodeToken =  JSON.parse(localStorage.getItem("decodedVP")!);
    }, 300)
  }
 
  getUserName() {
    let firstLetter = 'unknown', secondLetter = '';
    if (this.decodeToken?.given_name) {
        firstLetter = this.decodeToken.given_name.charAt(0).toUpperCase();
    }

    if (this.decodeToken?.family_name) {
        secondLetter = this.decodeToken.family_name.charAt(0).toUpperCase();
    }

    return {letters: firstLetter === 'unknown' ? 'UU' : `${firstLetter} ${secondLetter}`.trim(), fullName: firstLetter === 'unknown' ? 'unknown' : `${this.decodeToken.given_name}`.trim()};
  }

  redirectToWallet(){
    window.open(`${environment.WALLET_URL}/Credentials`,  '_blank');
  }

  changeKey() {
    this.dialog.open(ApiKeysModalComponent, {
      width: '300px'
    });
  }

  async logout() {

    localStorage.removeItem("logged");
    localStorage.removeItem("locations");
    localStorage.removeItem("decodedVP");
    localStorage.removeItem("apiKeyUserAgent");
    localStorage.removeItem("apiKeyCatalogue");
    this.cookiesService.deleteAll()
    this.logged = false;
    try {
      await this.keycloakService.logout().then(() => {
        this.router.navigate(['login']);
      }).catch((error) => {
        console.log('error', error);
      });
    } catch (error) {
      console.error('Error logging in', error);
    }

  }
  toggleSidenav() { }

  openDialog() {
    if (!localStorage.getItem("decodedVP")) {
        const dialogRef = this.dialog.open(DisclaimerDialogComponent, {
            width: "40%"
        });
    }
  }

  setDropdownState(isOpen: boolean): void {
    this.isDropdownOpen = isOpen;
  }
}
