import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-disclaimer-dialog',
  templateUrl: './disclaimer-dialog.component.html',
  styleUrls: ['./disclaimer-dialog.component.scss']
})
export class DisclaimerDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DisclaimerDialogComponent>) { }

  ngOnInit(): void {
  }

  onClose() {
    this.dialogRef.close()
  }

}
