import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-api-keys-modal',
  templateUrl: './api-keys-modal.component.html',
  styleUrls: ['./api-keys-modal.component.scss']
})
export class ApiKeysModalComponent {
  apiKeyUserAgent: string = '';
  apiKeyCatalogue: string = '';
  hideUserAgentZone = true;
  hideCatalogueZone = true;

  constructor(
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    ) { }

  ngOnInit() {
    this.apiKeyUserAgent = localStorage.getItem('apiKeyUserAgent') || '';
    this.apiKeyCatalogue = localStorage.getItem('apiKeyCatalogue') || '';
  }
  saveKeys() {
    localStorage.setItem("apiKeyUserAgent", this.apiKeyUserAgent)
    localStorage.setItem("apiKeyCatalogue", this.apiKeyCatalogue)

    this.dialog.closeAll();

    this.snackBar.open('API Keys saved successfully', 'Close', {
      duration: 3000
    });
  }
}
