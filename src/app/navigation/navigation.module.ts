import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationComponent } from './components/navigation/navigation.component';
import { MaterialModule } from '../shared/material/material.module';
import { AppRoutingModule } from '../app-routing.module';
import { DisclaimerDialogComponent } from './components/disclaimer-dialog/disclaimer-dialog.component';
import { ApiKeysModalComponent} from "./components/api-keys-modal/api-keys-modal.component";


@NgModule({
  declarations: [
    NavigationComponent,
    DisclaimerDialogComponent,
    ApiKeysModalComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    AppRoutingModule
  ],
  exports: [
    NavigationComponent
  ]
})
export class NavigationModule { }
