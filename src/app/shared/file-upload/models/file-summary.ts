export class FileSummary {
  loadedPart!: number;
  progress: number;
  name: string;
  size: number;
  type: string;
  content!: any;
  base64: string = '';

  constructor(
    name: string,
    size: number,
    type: string,
    progress: number,
    content: any,
    base64?: string
  ) {
    this.name = name;
    this.size = size;
    this.type = type;
    this.progress = progress;
    this.content = content;
    if(base64)
      this.base64 = base64;
  }
  isLoading(): boolean {
    return this.loadedPart !== this.size;
  }
  getLoadingPercent() {
    let pourcentLoaded = (this.loadedPart / this.size) * 100;
    return Number(pourcentLoaded / 100).toLocaleString(undefined, {
      style: 'percent',
      minimumFractionDigits: 2,
    });
  }
}
