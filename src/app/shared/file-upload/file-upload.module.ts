import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material/material.module';
import { FileUploadComponent } from './components/file-upload.component';
import {FileDragAndDropDirective} from "./directives/file-drag-and-drop.directive";

@NgModule({
  declarations: [FileUploadComponent, FileDragAndDropDirective],
  imports: [CommonModule, MaterialModule],
  exports: [FileUploadComponent],
})
export class FileUploadModule {}
