import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FileSummary } from '../models/file-summary';


@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
})
export class FileUploadComponent {
  files: FileSummary[] = [];

  @Input()
  file! : FileSummary;
  progress = 0;
  selectedFiles!: FileList;
  currentFile!: File;


  @Input()
  public message : string = 'service description verifiable presentation'

  @Input()
  public zone : string = '';

  @Input()
  public acceptedTypeName : string = 'JSON (Gaia-X)'

  @Input()
  public imgSrc : string = 'assets/images/upload-blue-big.svg'

  @Output()
  public submitted = new EventEmitter<FileSummary[]>();

  @Output()
  public submittedFile = new EventEmitter<FileSummary>();

  onFileDropped($event: Event) {
    const target = $event.target as HTMLInputElement;
    this.prepareFile(target.files![0])
    //this.prepareFilesList(target.files!);
  }

  selectFile($event: Event): void {
    const target = $event.target as HTMLInputElement;
    if (target.files) this.selectedFiles = target.files;
  }

  fileBrowseHandler($event: Event) {
    const target = $event.target as HTMLInputElement;
    if (target.files) this.prepareFilesList(target.files);
  }

  fileBrowseHandlerFile($event: Event) {
    const target = $event.target as HTMLInputElement;
    if (target.files) this.prepareFile(target.files[0]);
  }

  deleteFile() {
    this.submitted.emit(undefined);
  }

  uploadFilesSimulator(index: number) {
    setTimeout(() => {
      if (index === this.files.length) {
        this.submitted.emit(this.files);
        return;
      } else {
        const progressInterval = setInterval(() => {
          if (this.files[index].progress === 100) {
            clearInterval(progressInterval);
            this.uploadFilesSimulator(index + 1);
          } else {
            this.files[index].progress += 20;
          }
        }, 50);
      }
    }, 200);
  }

  prepareFilesList(files: FileList) {
    Array.from(files).forEach((item) => {
      let file = new FileSummary(
        item.name,
        item.size,
        item.type,
        0,
        item.arrayBuffer
      );
      this.files.push(file);
    });
    //this.uploadFilesSimulator(0);
  }


  async prepareFile(fileUploaded: File) {
      //this.files.push(file)

      let file = new FileSummary(
        fileUploaded.name,
        fileUploaded.size,
        fileUploaded.type,
        0,
        fileUploaded.arrayBuffer()
      );
      file.base64 = await this.loadFileAsBase64(fileUploaded, (ev) => {
        file.progress = ev.loaded;
      })
      this.file = file;
      this.submittedFile.emit(this.file)

    //this.uploadFilesSimulator(0);
  }

  // async loadFile(file: File) {
  //   this.file = file;
  //   this.fileSummary = new FileSummary(file.name, file.size, file.type);
  //   this.fileSummary.fileBase64 = await this.loadFileAsBase64(file, (ev) => {
  //   this.fileSummary!.loadedPart = ev.loaded;
  //   });
  //   this.fileSummary!.fileString = this.fileString;
  //   this.submitted.emit(this.fileSummary);
  // }

  loadFileAsBase64(
    data: Blob,
    onProgress?: (ev: ProgressEvent<FileReader>) => void
  ): Promise<string> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = () => {
        if (reader.result !== null) {
          let encoded = reader.result.toString().replace(/^data:(.*,)?/, '');
          if (encoded.length % 4 > 0) {
            encoded += '='.repeat(4 - (encoded.length % 4));
          }
          resolve(encoded);
        } else {
          reject('No result');
        }
      };
      reader.onerror = (error) => reject(error);
      if (onProgress != null) reader.onprogress = onProgress;
      reader.readAsDataURL(data);
    });
  }


  formatBytes(bytes: number, decimals?: number) {
    if (bytes === 0) {
      return '0 Bytes';
    }
    const k = 1024;
    const dm = decimals! <= 0 ? 0 : decimals || 2;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }
}
