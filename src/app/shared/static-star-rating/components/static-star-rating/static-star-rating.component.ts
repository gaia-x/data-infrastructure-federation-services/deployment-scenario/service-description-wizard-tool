import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-static-star-rating',
  templateUrl: './static-star-rating.component.html',
  styleUrls: ['./static-star-rating.component.scss']
})
export class StaticStarRatingComponent implements OnInit {

  @Input()
  value : number = 0;
  
  constructor() { }

  ngOnInit(): void {
  }

}
