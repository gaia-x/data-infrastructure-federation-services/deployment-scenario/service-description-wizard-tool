import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CircularProgressBarComponent } from './components/circular-progress-bar/circular-progress-bar.component';



@NgModule({
  declarations: [
    CircularProgressBarComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CircularProgressBarComponent
  ]
})
export class CircularProgressBarModule { }
