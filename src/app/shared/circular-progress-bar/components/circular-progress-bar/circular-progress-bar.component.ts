import { Component, HostBinding, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-circular-progress-bar',
  templateUrl: './circular-progress-bar.component.html',
  styleUrls: ['./circular-progress-bar.component.scss']
})
export class CircularProgressBarComponent implements OnInit {

  @Input()
  stage : number = 1;
  @Input()
  maxStage : number = 5;
  percentage : number = 0;

  @HostBinding('style.--target-degrees')
  degrees : string = '';

  constructor() { }

  ngOnInit(): void {
      this.percentage = this.stage/this.maxStage * 100
      this.degrees = ((180/100) * this.percentage).toString() + "deg"
  }

  ngOnChanges(changes: any) { 
    this.percentage = changes.stage.currentValue/this.maxStage * 100
    this.degrees = ((180/100) * this.percentage).toString() + "deg"
}

}
