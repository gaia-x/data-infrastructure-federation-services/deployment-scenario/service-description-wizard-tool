import { Injectable } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { Inject, Injector } from '@angular/core';
import {
  AuthConfig,
  NullValidationHandler,
  OAuthService,
} from 'angular-oauth2-oidc';
import { filter } from 'rxjs/operators';


@Injectable()
export class AuthConfigService {
  private _decodedAccessToken: any;
  private _decodedIDToken: any;
  get decodedAccessToken() {
    return this._decodedAccessToken;
  }
  get decodedIDToken() {
    return this._decodedIDToken;
  }

  constructor(
    private readonly oauthService: OAuthService,
    private injector : Injector,
    @Inject(APP_BASE_HREF) private baseHref: string
  ) {}

  load(): Promise<any> {
      return this.initAuth();
}

  initAuth(): Promise<any> {
    return new Promise<void>((resolveFn, rejectFn) => {

      let authConfig = new AuthConfig();
      authConfig.issuer = ""
      authConfig.redirectUri = ""
      authConfig.clientId = ""
      authConfig.responseType = ""
      authConfig.scope = ""
      authConfig.requireHttps = false;
      authConfig.disableAtHashCheck = true
      authConfig.showDebugInformation = true
      authConfig.dummyClientSecret = "secret"
      // set origin
      authConfig.redirectUri = this.baseHref;
      if (authConfig.redirectUri.startsWith('/'))
        authConfig.redirectUri =
          window.location.origin + authConfig.redirectUri;

      if (authConfig.issuer == null) authConfig.issuer = '/';
      if (authConfig.issuer.startsWith('/'))
        authConfig.issuer = window.location.origin + authConfig.issuer;

      // setup oauthService
      this.oauthService.configure(authConfig);
      this.oauthService.setStorage(localStorage);
      this.oauthService.tokenValidationHandler = new NullValidationHandler();

      // subscribe to token events
      this.oauthService.events
        .pipe(filter((e: any) => e.type === 'token_received'))
        .subscribe(() => this.handleNewToken());

      this.oauthService.events
        .pipe(filter((e) => e.type === 'session_terminated'))
        .subscribe(() => {
          console.log('The session has been terminated!');
          this.oauthService.logOut();
        });

      this.oauthService.events
        .pipe(filter((e) => e.type === 'token_refresh_error'))
        .subscribe(() => {
          console.log('An error occured on refresh token');
          this.oauthService.logOut();
        });

      this.oauthService.loadDiscoveryDocumentAndLogin().then((isLoggedIn) => {
        if (isLoggedIn) {
          this.oauthService.setupAutomaticSilentRefresh();
          resolveFn();
        } else {
          this.oauthService.initCodeFlow();
          rejectFn();
        }
      });
    });
  }

  private async handleNewToken() {
    this._decodedAccessToken = this.oauthService.getAccessToken();
    this._decodedIDToken = this.oauthService.getIdToken();
    localStorage.setItem('accessTokenId', this.decodedIDToken);
    localStorage.setItem('accessToken', this.decodedAccessToken);
  }
}
