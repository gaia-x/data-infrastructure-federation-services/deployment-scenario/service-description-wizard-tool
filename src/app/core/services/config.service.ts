import { HttpClient } from '@angular/common/http';
import { APP_INITIALIZER, Injectable } from '@angular/core';

export interface Config {
    did: string;
    apiExternalUrl: string;
}

@Injectable({
  providedIn: 'root',
})
export class ConfigService {
  config: any;

  constructor(private _http: HttpClient) {}

  async load() {
    let loadedData = sessionStorage.getItem('configData');
    if (loadedData == null || loadedData == '') {
      const configData = await this._http
        .get('assets/config/config.json')
        .toPromise();
      sessionStorage.setItem('configData', JSON.stringify(configData));
    } else {
      this.config = JSON.parse(loadedData);
    }
  }

  getApiUrl(): string {
    if(this.config)
    return this.config['apiUrl'];
    else return ''
  }

  getCompanyName() : string {
    if(this.config)
    return this.config['providerDesignation']
    else return ''
  }

  getNotarizationUrl() : string {
    if(this.config)
    return this.config['apiNotarizationUrl']
    else return ''
  }

  firstLetterUppercase(string : string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  getExternalApiUrl(): string {
    if(this.config)
      return this.config['apiExternalUrl']
    else return ''
  }

  getDid(): string {
    if(this.config)
    return this.config['did']
    else return ''
  }
}

export function ConfigFactory(config: ConfigService) {
  return async () => await config.load();
}

export function init() {
  return {
    provide: APP_INITIALIZER,
    useFactory: ConfigFactory,
    deps: [ConfigService],
    multi: true,
  };
}

const ConfigModule = {
  init: init,
};

export { ConfigModule };

