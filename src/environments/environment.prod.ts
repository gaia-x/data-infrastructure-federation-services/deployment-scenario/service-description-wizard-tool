// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  PROVIDER: "__PROVIDER__",
  FEDERATION: "__FEDERATION__",
  get CATALOGUE_URL() {
    return `https://federated-catalogue-api.${this.FEDERATION}.demo23.gxfs.fr`;
  },
  get WEB_CATALOGUE_URL() {
    return `https://webcatalog.${this.FEDERATION}.demo23.gxfs.fr`;
  },
  get LABELLING_URL() {
    return `https://labelling.${this.FEDERATION}.demo23.gxfs.fr`;
  },
  NOTARIZATION_URL: "https://notarization-service.demo23.gxfs.fr",
  COMPLIANCE_URL: "https://compliance.lab.gaia-x.eu/development",
  WALLET_URL: "https://wallet.waltid.demo23.gxfs.fr",
  keycloak: {
    url: 'https://keycloak.demo23.gxfs.fr',
    realm: 'gaiax',
    clientId: 'service-wizzard-dev'
  },
  PROVIDER_VCS: JSON.stringify([
    {
      "@context": [
        "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/credentials",
        "https://w3id.org/security/suites/jws-2020/v1"
      ],
      "@id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/legal-participant/legal-participant.json",
      "@type": [
        "VerifiableCredential"
      ],
      "credentialSubject": {
        "@context": [
          "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
        ],
        "gx:headquarterAddress": {
          "gx:countrySubdivisionCode": "FR-IDF"
        },
        "gx:legalAddress": {
          "gx:countrySubdivisionCode": "FR-IDF"
        },
        "gx:legalName": "AgDataHub",
        "gx:legalRegistrationNumber": {
          "id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/legal-participant/gaiax-legal-registration-number.json"
        },
        "id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/data.json",
        "type": "gx:LegalParticipant"
      },
      "issuanceDate": "2023-11-02T20:03:22.685809+00:00",
      "issuer": "did:web:agdatahub.provider.demo23.gxfs.fr",
      "proof": {
        "created": "2023-11-02T20:03:22.685809+00:00",
        "jws": "eyJhbGciOiAiUFMyNTYiLCAiYjY0IjogZmFsc2UsICJjcml0IjogWyJiNjQiXX0..PowyMgQYdhVHIJtyoVQwK5J5CMUupUQJnwcPkEP7aHP6UUAT8kr8iHV3uQhbNrnzwYEI0xnQojMBs56Yop6l4DtjJP8brx38IiPJ71fFxnOirqKvhFqXcJQyXlKhZjEczIGMJ8MlBlC6MJaS_2GQVIZEu6hbXcSRXpzX1wtMLEvfP7Wc_1TzPbXKzs4xbwqDe7ud0AWlsR4tpnf1EgVAFSQCn2Dnhchfeoe6cJfb9ZN7h0M9vGa9dD01lfcfY9_jQor5pYBdYoU3rr1KJmX8OCbWx4CJyDyMBSmZ8sN8Z9VxbUa-vKQOHhyMp_EipghxjsAaRXg_E5gltr6KlLsjiA",
        "proofPurpose": "assertionMethod",
        "type": "JsonWebSignature2020",
        "verificationMethod": "did:web:agdatahub.provider.demo23.gxfs.fr"
      }
    },
    {
      "@context": [
        "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/credentials",
        "https://w3id.org/security/suites/jws-2020/v1"
      ],
      "@id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/legal-participant/gaiax-terms-and-conditions.json",
      "@type": [
        "VerifiableCredential"
      ],
      "credentialSubject": {
        "@context": [
          "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
        ],
        "gx:termsAndConditions": "The PARTICIPANT signing the Self-Description agrees as follows:\n- to update its descriptions about any changes, be it technical, organizational, or legal - especially but not limited to contractual in regards to the indicated attributes present in the descriptions.\n\nThe keypair used to sign Verifiable Credentials will be revoked where Gaia-X Association becomes aware of any inaccurate statements in regards to the claims which result in a non-compliance with the Trust Framework and policy rules defined in the Policy Rules and Labelling Document (PRLD).",
        "id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/gaiax-terms-and-conditions.json",
        "type": "gx:GaiaXTermsAndConditions"
      },
      "issuanceDate": "2023-11-02T20:03:22.671059+00:00",
      "issuer": "did:web:agdatahub.provider.demo23.gxfs.fr",
      "proof": {
        "created": "2023-11-02T20:03:22.671059+00:00",
        "jws": "eyJhbGciOiAiUFMyNTYiLCAiYjY0IjogZmFsc2UsICJjcml0IjogWyJiNjQiXX0..B9S3IjUZsTX0wYl3PjNnZmaJdzTScFGDHGB7TxEG7N9SOgD4WAqJG5ZGfcynz6uXshyYAhCDpSgM7qu4oXLYONKbdlPFnmZ-_u2wgJqcc76B8YeIhtITw3bBMI_1JWMxXr5crXK3j-DGlG_Uel0YaUrSFhXDSiZBJgzBuxlGR5sJAJbm5nAD3Awb0USGEv37-ejEpC8BTtraUsqSCI3f9igfe1-hD-Ych5yfMztd-mc77h3RLK_oaSILW6t7hOrFAMZw8RaxqHULg0sNoqUQYrbRkX7_1zB0nF9COPabZVFqhUdKtf6SWtNpzE1fzItEnU5Gy5q0rfr4E1ishQRiBQ",
        "proofPurpose": "assertionMethod",
        "type": "JsonWebSignature2020",
        "verificationMethod": "did:web:agdatahub.provider.demo23.gxfs.fr"
      }
    },{
      "@context": [
        "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/credentials",
        "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/jws-2020"
      ],
      "@id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/67ba1105b4e91e380c3d22c4816532c27b92a9e92eacde22380ebbb6a1c93fe7/data.json",
      "@type": [
        "VerifiableCredential"
      ],
      "credentialSubject": {
        "@context": "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#",
        "gx:vatID": "FR31887921484",
        "gx:vatID-countryCode": "FR",
        "id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/legal-participant/gaiax-legal-registration-number.json",
        "type": "gx:legalRegistrationNumber"
      },
      "issuanceDate": "2023-12-19T15:42:42.805459+00:00",
      "issuer": "did:web:agdatahub.provider.demo23.gxfs.fr",
      "proof": {
        "created": "2023-12-19T15:42:42.805459+00:00",
        "jws": "eyJhbGciOiAiUFMyNTYiLCAiYjY0IjogZmFsc2UsICJjcml0IjogWyJiNjQiXX0..fgenuacSh_UVH3ikkjTfqSi9kWmxqZU25DeKW_jbg6Xz8Qp-e8W_Udq4v8BzcRiMaKedRs1OdxigJaLrU8sXv_okAeCLwWyilwcDQfve4UJjekI7LD8nLCaaHPvEJxBjYfWKhlQ_18tInrYVo03Q5F8PwNcbTpQYf_W7BzdPKDsTCEi-1Uxg-zheWA0LmH93i2IAMNWeqaDvhfta_KkWk5NFmvo4pYTb03PWtH0qYTNmp73MZuDNvlfs1vSO824OFVq_XsTELa6tzkAWLcR5uDF7NJkUl2tC78VyOMjbzGTsJc0EXiUxKT4YAU4Cs7j8Mnhep2s8jbZNLwsVGZFI8w",
        "proofPurpose": "assertionMethod",
        "type": "JsonWebSignature2020",
        "verificationMethod": "did:web:agdatahub.provider.demo23.gxfs.fr"
      }
    }
  ])
};
