{
  "@context": [
    "Specifies the JSON-LD context used to understand the terms in the document. Here, it refers to a credentials context defined by a certain schema."
  ],
  "@id": "This is the unique identifier of this verifiable presentation, likely a URL where the document can be retrieved",
  "@type": [
    "Indicates the type of the object, which in this case is a \"VerifiablePresentation\"."
  ],
  "proof": {
    "jws": "Contains the cryptographic proof for the presentation, including the signature type (JsonWebSignature2020), the verification method, and the signature itself (JWS).",
    "proofPurpose": "assertionMethod",
    "type": "JsonWebSignature2020",
    "verificationMethod": "verification method"
  },
  "verifiableCredential": [
    {
      "@context": [
        "Specifies the JSON-LD context for interpreting the terms within the document. "
      ],
      "@id": "The unique identifier of this verifiable credential, likely a URL where the credential can be retrieved or accessed.",
      "@type": [
        "Indicates the type of the object, here it is a \"VerifiableCredential\"."
      ],
      "credentialSubject": {
        "@context": [
          "Specifies the JSON-LD context for interpreting the terms within the document. "
        ],
        "aster-conformity:layer": "Indicates the layer of service",
        "gx:dataAccountExport": [
          {
            "gx:accessType": "Details how data can be accessed or exported (ex: digital)",
            "gx:formatType": "Details how data can be accessed or exported (ex: mime/png)",
            "gx:requestType": "Details how data can be accessed or exported (ex: email)"
          }
        ],
        "gx:dataProtectionRegime": [
          "Specifies the data protection regime (ex: GDPR2016)"
        ],
        "gx:description": "Provide details about the service offering",
        "gx:descriptionMarkDown": "Provide details about the service offering",
        "gx:isAvailableOn": [
          "Provide details about the service offering"
        ],
        "gx:keyword": [
          "Provide details about the service offering"
        ],
        "gx:name": "Provide details about the service offering",
        "gx:policy": "Provide details about the service offering",
        "gx:providedBy": {
          "id": "Provide details about the service offering"
        },
        "gx:termsAndConditions": {
          "gx:URL": "Provider terms and conditions",
          "gx:hash": "Hash of the provider terms and conditions content"
        },
        "gx:webAddress": "Provide details about the service offering",
        "id": "A unique identifier for the credential subject.",
        "type": "Specifies the type of the credential subject, here it is \"gx:ServiceOffering\"."
      },
      "issuanceDate": "The date and time when the credential was issued.",
      "issuer": "Identifies the entity that issued the credential, provided as a decentralized identifier (DID).",
      "proof": {
        "jws": "Contains the cryptographic proof for the presentation, including the signature type (JsonWebSignature2020), the verification method, and the signature itself (JWS).",
        "proofPurpose": "assertionMethod",
        "type": "JsonWebSignature2020",
        "verificationMethod": "verification method"
      }
    },
    {
      "@context": [
        "Specifies the JSON-LD context for interpreting the terms within the document. "
      ],
      "@id": "The unique identifier of this verifiable credential, likely a URL where the credential can be retrieved or accessed.",
      "@type": [
        "Indicates the type of the object, here it is a \"VerifiableCredential\"."
      ],
      "credentialSubject": {
        "aster-conformity:hasAssessedComplianceCriteria": [
          {
            "id": "Identifier of the validated criteria"
          },
          {
            "id": "Identifier of the validated criteria"
          }
        ],
        "aster-conformity:hasServiceOffering": "Service Offering Identifier linked to this claim"
      },
      "issuanceDate": "The date and time when the credential was issued.",
      "issuer": "Identifies the entity that issued the credential, provided as a decentralized identifier (DID).",
      "proof": {
        "jws": "Contains the cryptographic proof for the presentation, including the signature type (JsonWebSignature2020), the verification method, and the signature itself (JWS).",
        "proofPurpose": "assertionMethod",
        "type": "JsonWebSignature2020",
        "verificationMethod": "verification method"
      }
    },
    {
      "@context": [
        "Specifies the JSON-LD context for interpreting the terms within the document."
      ],
      "@id": "The unique identifier of this verifiable credential, likely a URL where the credential can be retrieved or accessed.",
      "@type": [
        "Indicates the type of the object, here it is a \"VerifiableCredential\"."
      ],
      "credentialSubject": {
        "@context": [
          "Defines the JSON-LD context for interpreting the terms within the document, establishing the framework for understanding the data fields."
        ],
        "gx:headquarterAddress": {
          "gx:countrySubdivisionCode": "Gives the location of the headquarters, here specified with a country subdivision code \"FR-IDF\", indicating a region in France."
        },
        "gx:legalAddress": {
          "gx:countrySubdivisionCode": "Provides the legal address."
        },
        "gx:legalName": "The legal name of the entity.",
        "gx:legalRegistrationNumber": {
          "id": "A unique identifier for the entity's legal registration number, given as a decentralized identifier (DID) URL."
        },
        "id": "A unique identifier for the credential subject.",
        "type": "Defines the type of the credential subject, here as \"gx:LegalParticipant\"."
      },
      "issuanceDate": "The date and time when the credential was issued.",
      "issuer": "Identifies the entity that issued the credential, provided as a decentralized identifier (DID).",
      "proof": {
        "jws": "Contains the cryptographic proof for the presentation, including the signature type (JsonWebSignature2020), the verification method, and the signature itself (JWS).",
        "proofPurpose": "assertionMethod",
        "type": "JsonWebSignature2020",
        "verificationMethod": "verification method"
      }
    },
    {
      "@context": [
        "Specifies the JSON-LD context for interpreting the terms within the document."
      ],
      "@id": "The unique identifier of this verifiable credential, likely a URL where the credential can be retrieved or accessed.",
      "@type": [
        "Indicates the type of the object, here it is a \"VerifiableCredential\"."
      ],
      "credentialSubject": {
        "@context": [
          " specifies the JSON-LD context relevant to the subject."
        ],
        "gx:termsAndConditions": "Details the terms and conditions agreed upon by the participant. This includes obligations to update descriptions in case of changes and the consequences of non-compliance with the Gaia-X Trust Framework and policy rules.",
        "id": "A unique identifier, likely a DID URL, for the terms and conditions document.",
        "type": "Defines the type of the credential subject, in this case, \"gx:GaiaXTermsAndConditions\"."
      },
      "issuanceDate": "The date and time when the credential was issued.",
      "issuer": "Identifies the entity that issued the credential, provided as a decentralized identifier (DID).",
      "proof": {
        "jws": " Includes the JSON Web Signature (JWS), detailing the signature type, the verification method, and the signature itself.",
        "proofPurpose": "Specifies the purpose of the proof, here noted as \"assertionMethod\".",
        "type": "The type of signature used, which is \"JsonWebSignature2020\"",
        "verificationMethod": "The method used for verification."
      }
    },
    {
      "@context": [
        "Specifies the JSON-LD context for interpreting the terms within the document."
      ],
      "credentialSubject": {
        "@context": "Specifies the JSON-LD context for interpreting the terms within the document.",
        "gx:vatID": "The VAT (Value Added Tax) identification number of the entity.",
        "gx:vatID-countryCode": "The country code associated with the VAT ID.",
        "id": "A unique identifier for the credential subject, likely a DID URL.",
        "type": "Specifies the type of the credential subject, which is \"gx:legalRegistrationNumber\"."
      },
      "evidence": [
        {
          "gx:evidenceOf": "gx:vatID",
          "gx:evidenceURL": "http://ec.europa.eu/taxation_customs/vies/services/checkVatService",
          "gx:executionDate": "2023-11-02T19:21:34.206Z"
        }
      ],
      "id": "The unique identifier of this verifiable credential, likely a URL where the credential can be retrieved or accessed.",
      "issuanceDate": "The date and time when the credential was issued.",
      "issuer": "Identifies the entity that issued the credential, provided as a decentralized identifier (DID).",
      "proof": {
        "jws": "Contains the cryptographic proof for the presentation, including the signature type (JsonWebSignature2020), the verification method, and the signature itself (JWS).",
        "proofPurpose": "assertionMethod",
        "type": "JsonWebSignature2020",
        "verificationMethod": "verification method"
      },
      "type": [
        "Indicates the type of the object, here it is a \"VerifiableCredential\"."
      ]
    }
  ]
}
