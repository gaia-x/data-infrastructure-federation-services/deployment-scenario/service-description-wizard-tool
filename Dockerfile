FROM node:latest as build

WORKDIR /usr/local/app
COPY ./ ./
RUN npm install
RUN npm run build


FROM nginx:1.23.1-alpine

COPY --from=build /usr/local/app/dist/gaia-x /usr/share/nginx/html
COPY --from=build /usr/local/app/api /usr/local/app/api

COPY ./nginx.conf /etc/nginx/
RUN apk add --update nodejs npm
WORKDIR /usr/local/app
#FIXME
RUN npm install json-server@^0.17.4
COPY ./startup.sh ./
RUN chmod +x ./startup.sh

EXPOSE 8080 3000
CMD [ "/bin/sh", "-c", "/usr/local/app/startup.sh" ]
