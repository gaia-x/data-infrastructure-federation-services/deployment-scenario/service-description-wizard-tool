# Description of the Service Description Wizard Tool

# Table of Contents
- 1: [Introduction](#introduction)
  - 1.1 [Presentation of the service description wizard tool](#wizard-tool-presentation)
  - 1.2 [Context](#context)
  - 1.3 [Objectives of the wizard tool](#objectives)
- 2: [Overview of the wizard tool](#overview-wizard-tool)
  - 2.1 [Add Service Description](#service-description)
  - 2.2 [Certifications loading](#certification)
  - 2.3 [Fill out the questionnaire](#questionnaire)
  - 2.4 [Form submission](#form)
  - 2.5 [Label Simulation](#label-simulation)
  - 2.6 [Claims signing](#claims-signing)
  - 2.7 [Compliance Request](#compliance-req)
  - 2.8 [Label Request](#label-req)
  - 2.9 [Publish to Catalogue](#publish)
- 3: [Technical Specification](#technical-pecification)
  - 3.1 [Technologies and tools used](#technologies)
  - 3.2 [Step to install the project locally](#step-install)
  - 3.3 [Technical details](#tech-details)
    - 3.3.1 [Certifications loading](#tech-certification)
    - 3.3.2 [Fill out the questionnaire](#tech-questionnaire)
    - 3.3.3 [Form submission](#tech-form)
    - 3.3.4 [Label Simulation](#tech-label-simulation)
    - 3.3.5 [Claims signing](#tech-claims-signing)
    - 3.3.6 [Compliance Request](#tech-compliance-req)
    - 3.3.7 [Label Request](#tech-label-req)
- 4: [Customizing the Wizard Tool for Service Providers](#customizing)
  - 4.1. [Environment setup for development](#setup-dev)
  - 4.2. [Environment setup for production](#setup-prod)
  - 4.3. [Adapt the Service Offering creation page](#adapt)
    - 4.3.1. [Adding a text field](#text-field)
- 5: [Authors and acknowledgment](#authors-and-acknowledgment)
- 6: [License](#licence)

## 1. Introduction <a name="introduction"></a>

### 1.1. Presentation of service description wizard tool<a name="wizard-tool-presentation"></a>
Wizard Tool is the entry point for service providers to describe and create their services. The Wizard Tool can be seen as a tool to help service providers creating their VPs and VCs which are mandatory to publish them to the Federated Catalogue with compliance requirements and Aster-x Label (based on Gaia-x Label).

### 1.2. Context<a name="contexte"></a>

In the context of the Gaia-X project, it's important for services to fit easily and correctly into the shared catalogue. This is where the Wizard Tool steps in, purpose-built to facilitate this integration for service providers.

The Wizard Tool stands as the entry point for service providers, allowing them to describe and craft their services in an effective and structured manner. It not only acts as a gateway for providers but also as a guide, aiding them in creating their VPs and VCs. These elements are crucial to ensure that services can be published in the Federated Catalogue while adhering to compliance requirements and achieving the Aster-x Label (based on Gaia-x Label).

The Wizard Tool has an easy-to-use design that makes the tricky steps of fitting in with Gaia-X's rules simpler. By following the steps outlined by the tool, providers can ensure that their services not only meet the high standards of Gaia-X (TrustFramework v22.10) but are also presented in an appealing and coherent manner to end users in the catalogue.

The adoption of the Wizard Tool by service providers guarantees a smooth transition and successful integration into the Gaia-X ecosystem. This makes users trust the system, knowing that each service listed in the federated catalogue adheres to a strict set of standards and criteria.

In summary, the Wizard Tool is an asset for any service provider looking to join the Gaia-X (TrustFramework v22.10) movement and share its services in the Federated Catalogue. It ensures not only compliance but also facilitates an end-to-end journey, culminating in the posting of the service on the Federated Catalogue.
### 1.3. Objectives of the wizard tool<a name="objectives"></a>
1. Serve as the entry point for service providers to describe and create their services.
2. Aid service providers in creating their VPs and VCs.
3. Ensure services meet the compliance requirements to be listed in the Federated Catalogue.
4. Help services achieve the Aster-x Label (based on Gaia-x Label).
5. Streamline and simplify the process of integration into the Gaia-X ecosystem (TrustFramework v22.10) through a unique and user-friendly design (UX).
6. Enhance user trust by ensuring that services adhere to Gaia-X's high standards (TrustFramework v22.10).


## 2. Overview of the wizard tool<a name="overview-wizard-tool"></a>

### 2.1. Add Service Description<a name="service-description"></a>

Once the user is connected to the wizard tool dedicated to his company, he can fill out all the service fields mandatory (Service Description) per service he wants to be registered to the federated catalogue.   
For instance:
- Name
- Description
- Type
- Layer
- Location
- References (certifications)
- Questionnaire (self assessed criteria)

Some fields and attributes are dependent on other components and ontology like Location, Participant, ServiceOffering, Provider... To do so, the wizard tool calls these services trought APIs.

![service_desc](./img/wizard.png)

### 2.2. Certifications loading<a name="certification"></a>
Since auditors or certification providers do not yet issue certifications in VC format properly interpreted in Gaia-X, users have two options. They can either directly add a VC of certification if they have one, or, if they only have a PDF (which can be self-signed), they must use a Notarization Service to transform this signed PDF certification document into a VC.
That's why Wizard Tool have to call Notarization API to do so, thanks to a pop-up allowing to drag and drop PDF.

![claims](./img/claims.png)

###  2.3. Fill out the questionnaire<a name="questionnaire"></a>
Some criteria related to label attribution can be self assessed, that's why user might fill out a questionnaire with three categories of questions (Contractual Governance, Transparency, European Control).
Answers are included thanks to the Notarization to an object contained on the VC LocatedServiceOffering.

![questionnaire](./img/questionnaire.png)

### 2.4. Form submission<a name="form"></a>
Once the user completes and validates the form, the subsequent steps in the workflow will transition to the main interface of the Wizard. This streamlined approach ensures that users can navigate and complete tasks efficiently, without unnecessary disruptions. The main interface of the Wizard has been designed to provide a seamless continuation of the process, facilitating a user-friendly experience throughout the entire journey.

![1on5](./img/1on5.png)
### 2.5. Label Simulation<a name="label-simulation"></a>
So that the user can know which label his service will be eligible for, a simulation button has been created. It allows user to send the previously obtained VP and to know the label. It's only a simulation, the compliance service is not called and no compliance VC is returned.
This simulation was not developed in the Wizard Tool, it's done with the [Catalogue Service](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/web-catalog-v2/-/blob/main/README.md).

![label_sim_button](./img/labelSim.png)
![simulate_label](./img/simulateLabel.png)

### 2.6. Claims signing<a name="claims-signing"></a>
Previous VP is unsigned, that's why user can download it in order to sign it.  
Then user has to upload his VP signed from his computer to the wizard tool. Wizard Tool stores his VP on agent.

![1on5_signing](./img/1on5SigningButton.png)
![signing_options](./img/signingOptions.png)

If you select to sign the whole Verifiable Presentation, you're invited to download and sign all the VCs in the VerifiableCredential field. And then, upload the new VP with all the VCs signed.
![signing_VP](./img/signVP.png)

Otherwise, in the wizard we have created a service that help you to sign all the VC by a user-friendly interface. You can download and sign each VC one by one, the Wizard will automatically recreate the VP for you when you submit all the requested VC signed.
![signing_VP](./img/signEachVC.png)

After signing, the service can be passed to the compliance service by using the compliance button right after the 2/5 indication.
![2on5](./img/2on5.png)

### 2.7. Compliance Request<a name="compliance-req"></a>
The user can now submit the object to the compliance service to ensure it meets the required standards and regulations.

![2on5_compliance](./img/2on5ComplianceButton.png)
If your service is compliant, then it will be marked with 3/5.
![3on5](./img/3on5.png)

### 2.8. Label Request<a name="label-req"></a>
The user can now presented to labelling service the VP in order to have a label associated with the service.
![3on5_label](./img/3on5ReqLabel.png)

### 2.9. Publish to Catalogue<a name="publish"></a>
Once all the necessary steps are completed and the service has been verified for compliance, the final action is to publish it. With everything in order, the new service is then smoothly integrated and made available on the Federated Catalogue, marking the successful conclusion of the procedure and offering it to a broader audience within the Gaia-X ecosystem.
![3on5_publish](./img/3on5Publish.png)
![4on5](./img/4on5.png)
## 3. Technical Specification<a name="technical-pecification"></a>

### 3.1. Technologies and tools used<a name="technologies"></a>
Wizard Tool is an application developed in Angular with a nodejs server and a json server to manage locally the different objects.

#### Programming languages used
1. Angular  
2. NodeJS

#### The tools used

1. Wallet (compatible with OID4VC [cf. OpenID for Verifiable Credential Issuance](https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0.html))
2. Keycloak
3. user-agent

Wizard Tool entrypoint is available for each service provider on specific address such as : https://{wizard-tool}.{PROVIDER_NAME}.{demo23.gxfs.fr} (ie. https://wizard-tool.dufourstorage.demo23/gxfs.fr/).

### 3.2. Step to install the project locally<a name="step-install"></a>

####  Step 1

- Cloning the GitLab directory:

Start by opening your terminal (or command prompt) and
navigate to the directory where you want to clone the project.

Use the following command to clone the GitLab directory:

```
git clone https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/service-description-wizard-tool.git
```
####  Step 2
Installing the dependencies :

Once the project has been cloned, navigate to the project directory and install the necessary dependencies using the command :

```  
  npm install
```
####  Step 3

Starting the Angular project:

Once all the dependencies are installed, you can start the project locally using the command :
```
npm start
```
This command starts a development server and opens a new browser window with your project running.

####  Step 4

Once the development server is running, you can access the
catalogue in your browser at http://localhost:4200.

### 3.3. Technical details<a name="tech-details"></a>
In the following section, we will outline the technical specifications of our system. We aim to provide a clear and concise breakdown for those involved in its implementation and maintenance. It's essential to grasp these details to ensure smooth integration and operation within our business environment.
  
Additionally, to ensure coherence and ease of navigation, the technical sections will bear the same titles as the functional sections. This alignment facilitates a straightforward correlation between functionality and its technical underpinnings.
#### 3.3.1. Certifications loading<a name="tech-certification"></a>

For instance, first certification have to call ***create-vc*** routes on notarization with a json object as argument including name of the participant, certification type, base64 encoded string of the pdf document and the *ServiceOffering* object being used.
```
-> output
POST https://notarization-service.abc-federation.gaia-x.community/api/create-vc
ARG json object
{
	"participant": "dufoustorage",
	"certificationType": "ISO 27001",
	"b64file": "123456",
	"serviceOffering": ServiceOffering_JsonObject,
}

<- input
{
	"result": "OK/NOK", 
	"message": string, 
	"vc": LocatedServiceOffering_VC (signed)
}
```
Wizard has to store localy (or in his PCM component) the returned VC which is a LocatedServiceOffering.

Next certifications are loaded in the same way, which allows to enrich the LocatedServiceOffering VC with all the certifications attached to the service being described.
```
-> output
POST https://notarization-service.abc-federation.gaia-x.community/api/create-vc
ARG json object
{
	"participant": "dufoustorage",
	"certificationType": "ISO 27001",
	"b64file": "123456",
	"locatedServiceOffering": LocatedServiceOffering_VCObject,
}

<- input
{
	"result": "OK/NOK", 
	"message": string, 
	"vc": LocatedServiceOffering_VC (signed and concatenated with previous one)
}
```
As a result, user has an VC object LocatedServiceOffering which contains all ComplianceReferences objects related to his certifications properly loaded in the Wizard Tool, thanks to the Notarization Service.

#### 3.3.2. Fill out the questionnaire<a name="tech-questionnaire"></a>

Questionnaire answers example:
```
-> ie. questionnaire answers
[
    {
        "section": "Contractual governance",
        "criterion": "Criterion 1: The provider shall offer the ability to establish a legally binding act. This legally binding act shall documented",
        "val": true,
        "relevantFor": [
            "1",
            "2",
            "3"
        ]
    },
    ...
    {
        "section": "European Control",
        "criterion": "Criterion 61: The provider shall not access customer data unless authorised by the customer or when the access is in accordance with EU/EEA/member state law",
        "val": false,
        "relevantFor": [
            "1",
            "2",
            "3"
        ]
    }
]
```
#### 3.3.3. Form submission<a name="tech-form"></a>
When the form is fully completed the user can submit it to receive the associated VP. It's done through a call to the notarization API **/create-vp/**.
```
-> input
POST https://notarization-service.abc-federation.gaia-x.community/api/create-vp
ARG json object
{
	"participant": "dufourstorage",
	"locatedServiceOffering": LocatedServiceOffering_VC (signed),
	"questionnaire": QuestionnaireArray_JsonObject
}

<- output
{
	"result": "OK/NOK", 
	"message": string, 
	"vp": LocatedServiceOffering_VP (unsigned, to be signed)
}
```
#### 3.3.4. Label Simulation<a name="tech-label-simulation"></a>

URL to the catalogue label simulation :
https://webcatalog.aster-x.demo23.gxfs.fr/labelling/{did}

#### 3.3.5. Claims signing<a name="tech-claims-signing"></a>

The user-agent of a provider is a good option to sign the VCs and VPs. For instance, to sign with aster-x user-agent you can use this API 
  
PUT `https://aster-x.demo23.gxfs.fr/api/vc-request`  

Header:
```json 
{
  "x-api-key": "key",
  "Content-type": "application/json"
}
```
And put the credentialSubject of the VC or to sign in the body of the request.
  
    
To sign a VerifiablePresentation the url is `https://aster-x.demo23.gxfs.fr/api/vp-request`  
And the body wanted is the array of verifiableCredential, contained in verifiableCredential field.

#### 3.3.6. Compliance Request<a name="tech-compliance-req"></a>
The VP containing multiple VCs is then presented to compliance service in order to have a compliance VC object.
```
For instance,
curl -X POST -H "Content-Type: application/json" -d @vp_signed.json  https://compliance.lab.gaia-x.eu/development/api/credential-offers

-> input
POST https://ruleschecker.abc-federation.gaia-x.community/rules/compliance/complianceServiceOffering
ARG file vp_signed.json

<- output
{
    "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://w3id.org/security/suites/jws-2020/v1",
        "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
    ],
    "type": [
        "VerifiableCredential"
    ],
    "id": "https://compliance.lab.gaia-x.eu/development/credential-offers/b404826e-3f37-4fb3-adb3-d9af3cad2d37",
    "issuer": "did:web:compliance.lab.gaia-x.eu:development",
    "issuanceDate": "2023-11-08T19:19:43.496Z",
    "expirationDate": "2024-02-06T19:19:43.496Z",
    "credentialSubject": [
        {
            "type": "gx:compliance",
            "id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/5c9953e3bdbba28efb25e2aacb91385ace91fc56c0cf058f7da132afa7d96456/data.json",
            "gx:integrity": "sha256-065ce100c79f983c2ed0a68e225a99f617f18dcf656bcd19ea8a5edf51f3415c",
            "gx:integrityNormalization": "RFC8785:JCS",
            "gx:version": "22.10"
        },
        {
            "type": "gx:compliance",
            "id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/271a43ecb329cb34e488519b5a520fe7bbbcf3568f10fa24928bb02b8e90a33c/data.json",
            "gx:integrity": "sha256-4c11c4a311e9c5e7fcf89ee6652411980f9db2c84fc99db5e47eccad4dcecf16",
            "gx:integrityNormalization": "RFC8785:JCS",
            "gx:version": "22.10"
        },
        {
            "type": "gx:compliance",
            "id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/legal-participant/legal-participant.json",
            "gx:integrity": "sha256-ddca434abaa86626d55cb6397b25972e50c0fe1634affad304c85b5bc709c966",
            "gx:integrityNormalization": "RFC8785:JCS",
            "gx:version": "22.10",
            "gx:type": "gx:LegalParticipant"
        },
        {
            "type": "gx:compliance",
            "id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/legal-participant/gaiax-terms-and-conditions.json",
            "gx:integrity": "sha256-53e99624425715d31b0414cb1f7e09ca7a62ee69982b14936f63d926bdbc4009",
            "gx:integrityNormalization": "RFC8785:JCS",
            "gx:version": "22.10",
            "gx:type": "gx:GaiaXTermsAndConditions"
        },
        {
            "type": "gx:compliance",
            "id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/legal-participant/gaiax-legal-registration-number.json",
            "gx:integrity": "sha256-36a92d19eb17850dd9c7e7c3c1995f2d7a8feb2ec4632c7ee805fefb3e62b8ca",
            "gx:integrityNormalization": "RFC8785:JCS",
            "gx:version": "22.10",
            "gx:type": "gx:legalRegistrationNumber"
        }
    ],
    "proof": {
        "type": "JsonWebSignature2020",
        "created": "2023-11-08T19:19:43.506Z",
        "proofPurpose": "assertionMethod",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..g91GZjkkzbaEPwg8B39uQr0Hfy7TvYKTxx7oqwRtSH1RuP3Zz9qv6Jij43ZW-t77MWBp5ykcPa6KvZ52kL3MaEWitOS8bR8kdTgeWYORvBvwu2wDPPurJPfuL_sc_WGGfqmeGlknYIB6aj39HvP-E2IEWBjj0HP_LKHEnv1U-7aFc-VbZwJQTlXvYYmHk3Lzg4b3-YPzV6rmJaT7DgtjNBMkqJ4sm-dIo9-PbfZTFoHdDBNGSqZLJZjkj1PiqWQuQ6ak3RpsflU7CLo9eVMbSE2xPL21raS9vuSBspimmsuymgM4J1omrtQ5Uc6hXPDa5Se8garT28UeBhW7g0brlw",
        "verificationMethod": "did:web:compliance.lab.gaia-x.eu:development#X509-JWK2020"
    }
}
```
If compliance return an HTTP 2XX, VP seems to be valid, and Wizard Tool change service status.

#### 3.3.7. Label Request<a name="tech-label-req"></a>
Output of labelling api :
```
{
    "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://w3id.org/security/suites/jws-2020/v1"
    ],
    "@type": [
        "VerifiableCredential"
    ],
    "@id": "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/vc/493fb62b0ea36dd5adafecde89fc1fd64cc45745a3e50cabad001cd484d24600/data.json",
    "issuer": "did:web:aster-x.demo23.gxfs.fr",
    "credentialSubject": {
      "@context": [
          "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-conformity"
      ],
      "id": "did:web:dufourstorage.provider.demo23.gxfs.fr:granted-label/25b61f60-15fb-48e9-8e41-19caa2daa616/data.json",
      "type": "aster-conformity:grantedLabel",
      "vc": "did:web:aruba.provider.demo23.gxfs.fr:participant:2fcc9256ec1186625790e592f0427f659ed67e297001c1cf06e47ce1aa12c0a1/vc/f1be197f126ee17c050507fab37083d8f3e05a45780424b66b058fece5ac72e9/data.json",
      "label": {
          "hasName": "Gaia-X label Level 3",
          "id": "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/compliance-label/42b03421cde76b84ff90b7eb6cb1a2821ad6388bdb15b3d67cb73fd5c508da0b/data.json"
      },
      "proof-vc": {
          "type": "JsonWebSignature2020",
          "proofPurpose": "assertionMethod",
          "verificationMethod": "did:web:dufourstorage.provider.dev.gaiax.ovh",
          "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..hwUMHYq6tvx0nEBxywVz7wEZ_Pq5wu-LJITcmQ8--5oAmkIABz7x768P9vndt-U7UeCSXjEQ-2PCTxV4io8POp2gRsjNS6LrDg34e3lVvAL8jogl-18e_bFpcomdW2ofvgER38sedHW-UyiOUMDvjslIJVqKq5dJGuE0IZM9UD7YUrWwyLCQbmk1I4ZudvBX5iHSFpGSydJfju3T8IykPoYhPOSswr7K9nZCJcRBnK4Mntfocjz0QwGwpTNiDGwv1Xpp5DSuFxYxiI_-9LvSt-Ms-aUBFbmskrQbwGB1FcPnm0dlAreMD8MEbFeonb9tGunkZImzU65FrqAybUkDcg"
      }
    },
    "issuanceDate": "2023-10-31T10:25:17.616243+00:00",
    "proof": {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:aster-x.demo23.gxfs.fr",
        "created": "2023-10-31T10:25:17.616243+00:00",
        "jws": "eyJhbGciOiAiUFMyNTYiLCAiYjY0IjogZmFsc2UsICJjcml0IjogWyJiNjQiXX0..KPdDI0JXZCf4CoaHoKa8TgWfk4tYQGXXt-kplRpqZ3GiEVzfh5DKp80YUbolusvpfKZ997OnJVO4GiuNoRs_z93oOgyYrDoWIpj82LWbZUobit3s20V0meT1cTqSpQwHI57IEEMBNnPxvkV0Vy4pqBhIUYFvOCYU7ytaZZ02H5xNR4H2IH6YcL6p8r2zScnOEpZysMirmpLBQt_aeOblhjIOltMwkuLppuUgyDIyVRhTFWYXxw6vCubVeq3ut-T1yYQR8sSuUhsFVnNadF9ds4oOVoi98PDIWPKzawxrY3hgyewZLwK5CfuEG8-rIIyaaR5CqUW0wrqyJ1aLPXAYbQ"
    }
}
```

## 4. Customizing the Wizard Tool for Service Providers <a name="customizing"></a>

This section of our documentation is specifically adapted to the needs of service providers looking to adapt and personalize the Wizard Tool to better align with their unique operational requirements and service offerings.
From basic adjustments to more complex functional integrations, this chapter aims to provide a comprehensive understanding of the customization capabilities of the Wizard Tool.

### 4.1. Environment setup for development <a name="setup-dev"></a>
Environmental variables serve as key configuration parameters. In development environment, these variables often include Provider name, Federation name, URL, and other critical settings that differ between development and production environments.

Path to development variable: **src/environments/environment.ts**
``` typescript
export const environment = {
  production: false,
  PROVIDER: "agdatahub", //Provider name
  FEDERATION: "aster-x", //Federation name
  get CATALOGUE_URL() { //Federated Catalogue URL based on FEDERATION name
    return `https://federated-catalogue-api.${this.FEDERATION}.demo23.gxfs.fr`;
  },
  get WEB_CATALOGUE_URL() { //Web Catalogue URL based on FEDERATION name
    return `https://webcatalog.${this.FEDERATION}.demo23.gxfs.fr`;
  },
  get LABELLING_URL() { //Labelling service based on FEDRATION name
    return `https://labelling.${this.FEDERATION}.demo23.gxfs.fr`;
  },
  NOTARIZATION_URL: "https://notarization-service.demo23.gxfs.fr", //Notarization URL
  COMPLIANCE_URL: "https://compliance.lab.gaia-x.eu/development", //Compliance URL
  WALLET_URL: "https://wallet.waltid.demo23.gxfs.fr", //Wallet URL
  //Keycloak configuration
  keycloak: {
    url: 'https://keycloak.demo23.gxfs.fr',
    realm: 'gaiax',
    clientId: 'service-wizzard-dev'
  },
  //Tab of VCs contaning the legalRegistrationNumber VC, GaiaXTermsAndConditions VC and LegalParticipant VC
  PROVIDER_VCS: JSON.stringify([
        {
            //LegalParticipant
        },
        {
            //GaiaXTermsAndConditions
        },
        {
            //legalRegistrationNumber
        }
    ])
};
```
### 4.2. Environment setup for production <a name="setup-prod"></a>
In the realm of production environments, managing environment variables becomes even more critical, as these settings directly impact the live services and applications. A significant portion of these variables will be managed through Kubernetes deployment files, providing a robust and scalable approach to configuration management in production.

Path to production variable: **src/environments/environment/prod.ts**
``` typescript
export const environment = {
  production: true,
  PROVIDER: "__PROVIDER__", //Provider name
  FEDERATION: "__FEDERATION__", //Federation name
  get CATALOGUE_URL() { //Federated Catalogue URL based on FEDERATION name
    return `https://federated-catalogue-api.${this.FEDERATION}.demo23.gxfs.fr`;
  },
  get WEB_CATALOGUE_URL() { //Web Catalogue URL based on FEDERATION name
    return `https://webcatalog.${this.FEDERATION}.demo23.gxfs.fr`;
  },
  get LABELLING_URL() { //Labelling service based on FEDRATION name
    return `https://labelling.${this.FEDERATION}.demo23.gxfs.fr`;
  },
  NOTARIZATION_URL: "https://notarization-service.demo23.gxfs.fr", //Notarization URL
  COMPLIANCE_URL: "https://compliance.lab.gaia-x.eu/development", //Compliance URL
  WALLET_URL: "https://wallet.waltid.demo23.gxfs.fr", //Wallet URL
  //Keycloak configuration
  keycloak: {
    url: 'https://keycloak.demo23.gxfs.fr',
    realm: 'gaiax',
    clientId: 'service-wizzard-dev'
  },
  //Tab of VCs contaning the legalRegistrationNumber VC, GaiaXTermsAndConditions VC and LegalParticipant VC
  PROVIDER_VCS: JSON.stringify([
        {
            //LegalParticipant
        },
        {
            //GaiaXTermsAndConditions
        },
        {
            //legalRegistrationNumber
        }
    ])
};
```

At this point the PROVIDER and FEDERATION variables, need to be changed in the kubernetes files.

**deployment/packages/overlays/demo23-participants/PROVIDER/kustomization.yaml**
``` yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
  - ../../demo23-cluster
namespace: agdatahub
patches:
  - patch: |-
      - op: replace
        path: "/spec/rules/0/host"
        value: "wizard-tool.agdatahub.provider.demo23.gxfs.fr"
      - op: replace
        path: "/spec/tls/0/hosts"
        value: ["wizard-tool.agdatahub.provider.demo23.gxfs.fr"]
      - op: replace
        path: "/spec/tls/0/secretName"
        value: "ingress-wizard-tool-tls"
    target:
      kind: Ingress
      name: wizard-tool
  - patch: |-
      - op: replace
        path: "/spec/template/spec/containers/0/env/2/value"
        value: "Provider name"
      - op: replace
        path: "/spec/template/spec/containers/0/env/3/value"
        value: "Federation name"
    target:
      kind: Deployment
      name: wizard-tool
```

### 4.3. Adapt the Service Offering creation page <a name="adapt"></a>
Adapting the Wizard Tool code to facilitate the creation of service offerings is a step for service providers aiming to expand and customize their services. This section guides you through the process of modifying the Wizard Tool to effectively create and manage diverse service offerings.

#### 4.3.1. Adding a text field <a name="text-field"></a>

To add a text field go to form component html at : **src/app/service-administration/components/service-administration-form/service-administration-form.component.html**
``` typescript
<mat-form-field appearance="standard">
  <mat-label>FieldName</mat-label>
  <textarea matInput matTextareaAutosize placeholder="Field PlaceHolder" matAutosizeMinRows=1
    matAutosizeMaxRows=5 formControlName="FormControlerName"></textarea>
</mat-form-field>
```

Then add your custom field in the service-description.ts at: **src/app/service-administration/models/service-description.ts**

Finally add it in these form component controller functions at: **src/app/service-administration/components/service-administration-form/service-administration-form.component.ts**
  - constructor
  - populateFormOnEdit
  - createSo
  - editSo
  - save

## 5. Authors and acknowledgment <a name="authors-and-acknowledgment"></a>
GXFS-FR

## 6. License <a name="licence"></a>
The Wizard Tool Service (developed for the Gaia-X Federated Services Summit 2022 Demonstration) is delivered under the terms of the Apache License Version 2.0.
